Clock.bpm = 73

Root.default.set(var([0,2],256))

note = PRand(Scale.default,seed=5)
note2 = PRand(Scale.default,seed=7)

var.brks = var([1,0],[0,32,64,64,128])

s1 >> drone(oct=3,dur=32,lpf=400,room=2/4,mix=1/2,amplify=2/3*var.brks,amp=1)

s2 >> hoover([0,2],oct=3,dur=16,slide=[0,0,0,[0,1/4,0,1/2]],lpf=400,room=2/3,mix=1/2,amplify=linvar([3/6,2/6],64)*var.brks,amp=1)

s3 >> vibass(P[:2],oct=4,dur=4,lpf=400,hpf=160,verb=4/5,room=2/3,mix=1/2,pan=(-2/3,2/3),amplify=P[1,0,0,1,0,1,1]*2/9,amp=1)

s4 >> rsin(oct=4,dur=16,sus=8,lpf=expvar([2200,900],64),amplify=expvar([1/8,4/5],[16,0])*var.brks,amp=1)

s5 >> chipsy([0,1,0,2],oct=4,dur=32,formant=3,chop=16,tremolo=3,room=2/3,mix=1/2,pan=(-3/4,3/4),amplify=var([0,3/12],64)*var.brks,amp=1)

s6 >> dafbass(s5.degree,oct=4,dur=4,formant=5,lpf=900,amplify=4/8*var.brks,amp=1)

s7 >> flute(note,oct=4,dur=PRand([2,4,8]),tremolo=PRand([0,2,3]),room=4/5,mix=1/3,amplify=4/8,amp=1)

s8 >> harp(note2,oct=(5,6),dur=1/4,lpf=expvar([0,1300],64),room=4/5,mix=1/2,amplify=4/7,amp=1)

s9 >> borgan(s5.degree,oct=6,dur=2,sus=1,chop=var([0,1],32),room=2/3,mix=1/3,amplify=linvar([1/10,2/10],64),amp=1).offbeat()


b1 >> play("V",dur=2,delay=[0,0,0,1/2],sample=25,room=1/2,mix=1/3,amplify=2/3*var.brks,amp=1)

b2 >> play("o",dur=2,delay=[1,1,3/4,3/2],sample=25,room=1/2,mix=1/3,amplify=2/7*var.brks,amp=1)

b3 >> play("-",dur=1/2,sample=25,amplify=4/8*var.brks,amp=1).rarely("stutter",2)

b4 >> play("S",dur=1,sample=3,room=3/4,mix=1/2,amplify=1/5*var.brks,amp=1)

b5 >> play("s",dur=1,delay=1/2,sample=2,room=2/5,mix=1/2,amplify=3/7,amp=1)

b6 >> play("i",dur=2,delay=[0,1/4,1/2,0,3/4],sample=4,room=4/5,mix=1/2,amplify=2/5*var.brks,amp=1)

b7 >> play("f",dur=1,delay=var([1/2,1/4],[24,8]),rate=[2/3,1,4/5,6/5],sample=3,room=2/3,mix=1/2,amplify=3/8*var.brks,amp=1)



print(SynthDefs)
