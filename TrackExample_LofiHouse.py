#Lofi House Track Example

#Tempo
Clock.bpm=120
Clock.set_time(0)
#Root note
Root.default.set(5)
#Default scale
Scale.default=Scale.melodicMinor
print(Scale.default)
#Notes
bassla=[1,1,1,1,1,1,1,3,0,0,0,0,0,0,0,-1,0,0,3,3,3,3,7,1,1,3,0,0,0,0,1,-1,0,-1,0,0]
basslb=[1,1,1,1,1,0,0,0,0,0,4,4,4,4,4,3,3,3,3,3]
melodya=[10,5,4,5,10,5,4,5,10,5,4,5,10,5,4,5]
#SYNTHS
s1 >> bass(bassla, oct=5, dur=[1,1/4,3/4,3/4,3/4,5/2,1,1,1,1/4,3/4,3/4,3/4,1,1/2,1,1,1], sus=3/4, lpf=180, amplify=5/8, amp=1)
s2 >> lazer(5, oct=(3,4), dur=[8,4,4], sus=8, lpf=1600, drive=var([linvar([0,1/20],4),0],[8,8]), formant=var([3,2,1]), chop=16, room=3/4, mix=1/2, pan=(-2/3,2/3), amplify=3/5, amp=1)
s3 >> space([3,5], oct=(2,3), dur=8, sus=8, formant=2, glide=3/5, glidedelay=1/8, room=3/5, mix=1/3, amplify=4/5, amp=1).spread()
s4 >> bell(Pvar([0,3,5,7],1/8), oct=4, dur=var([1/8,1],[1/2,31/2]), sus=8, delay=1, lpf=500, formant=4, echo=1/8, echotime=1/12, room=1, mix=1/2, amplify=var([3/8,0],[1/2,31/2]), amp=1)
s5 >> orient(-2,oct=linvar([7,8],[4,0]),dur=1/4,sus=1/2,formant=3,lpf=expvar([2300,80],[2,0]),room=3/4,mix=1/2,amplify=5/7,amp=1)
s6 >> bass(basslb, oct=5, dur=[1/4,3/4,3/4,3/4,11/2], delay=1, sus=3/4, lpf=180, amplify=5/8, amp=1)
s7 >> marimba(melodya, oct=6, dur=1, sus=1/4, formant=2, lpf=900, room=3/5, mix=1/3, amplify=7/5, amp=1)
s8 >> scratch(oct=7, dur=4, sus=1, formant=0, chop=4, amplify=4/5, amp=1)
s9 >> bell(Pvar([7,5,3,0],1/8), oct=(4,5), dur=var([1/8,1],[1/2,31/2]), sus=8, delay=1, lpf=500, formant=4, echo=1/8, echotime=1/12, room=1, mix=1/2, amplify=var([3/8,0],[1/2,31/2]), amp=1)
#SAMPLES
b1 >> play("X", dur=1, sample=0, rate=5/6, lpf=1400, amplify=2, amp=1).every(32, "stutter", 2) 
b2 >> play("C", dur=1, sus=2, rate=1, sample=1, coarse=3, room=1/2, mix=1/2, amplify=3/5, amp=1)
b3 >> play("#",dur=4, sample=1, room=4/5, mix=1/3, pan=PWhite(-1,1), amplify=3/4, amp=1)
b4 >> play("o",dur=1,delay=3/4,sample=4,rate=7/5,lpf=2400,amplify=1,amp=1)
b5 >> play("t",dur=1/4,delay=PRand([0,1/2,0,0,1/2,0,3/4,0,1/2,0],1/2),lpf=900,pan=(-2/3,2/3),amplify=4/5,amp=1) 
b6 >> play("*", dur=2, delay=0, sample=1, rate=3/4, shape=1/8, pshift=3, lpf=2400, amplify=1, amp=1).every(16, "stutter", dur=3/2)
b7 >> play(".nNn", dur=1/4, sample=3, rate=6/5, lpf=4200, pshift=-5, amplify=3/5, amp=1)
b8 >> play("S",dur=1/4, rate=1, sample=4, pshift=var([-2,0],[1/2,7/2]), formant=4, lpf=linvar([400,3000],[8,0]), amplify=var([7/5,1],[1/2,7/2]), amp=1)
b9 >> play("s", dur=1/2, sample=3, rate=[7/5,1], lpf=3400, amplify=6/5, amp=1)
b0 >> play("~", dur=1, sample=2, formant=4, room=1, mix=1/2, amplify=3/7, amp=1)
print(480-256-32)
#SYTNHS TIMING
s1.amp=var([1,0],[0,128,64,96,64,128])
s2.amp=var([1,0],[8,24,8,24,360,24,8,24])
s3.amp=var([1,0],[8,24,8,24,360,24,8,24])
s4.amp=var([1,0],[0,64,128,96,128,64])
s5.amp=var([1,0],[0,208,24,8,24,8,24,8,24,8,16,128])
s6.amp=var([1,0],[0,224,64,192])
s7.amp=var([1,0],[0,344,8,128])
s8.amp=var([1,0],[0,128,4,156,4,188])
s9.amp=var([1,0],[0,256,32,192])
#SAMPLES TIMING
b1.amp=var([1,0],[192,64,224,0])
b2.amp=var([1,0],[0,192,8,56,8,56,8,56,8,88])
b3.amp=var([1,0],[0,128,4,156,4,188])
b4.amp=var([1,0],[0,20,1,11])
b5.amp=var([1,0],[0,288,32,32,32,96])
b6.amp=var([1,0],[0,32,94,2,286,2,32,32])
b7.amp=var([1,0],[62,2,62,2,64,64,158,2,30,2,30,2])
b8.amp=var([1,0],[96,2,6,2,2,2,2,2,6,2,2,2,34,32,32,64,32,64,32,2,6,2,2,2,2,2,6,2,2,2,34])
b9.amp=var([1,0],[0,44,4,12,4,32,30,34,32,48,48,32,64,44,4,12,4,32])
b0.amp=var([1,0],[0,256,32,32,32,128])

print(SynthDefs)

print(Attributes)
