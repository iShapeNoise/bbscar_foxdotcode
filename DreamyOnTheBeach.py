#FoxDot

Clock.bpm = 80
Root.default.set(0)
#NOTEZ
Scale.default.set=Pvar([Scale.egyptian, Scale.mixolydian], 64)
scale=Scale.default
note=PRand(scale, seed=7)
note2=[0, note, PRand(scale, seed=9)] 
note3=Pvar([[2, 3, note], [2, note, note]], 16)
#TIMING
var.int=var([1, 0], [64, 0, 0, 64, 0, 64, 0, 128, 0, 64, 0, 64, 0, 128, 0, 64])
var.brk=var([1, 0], [0, 64, 64, 0, 0, 64, 0, 128, 64, 0, 0, 64, 0, 128, 0, 64])
var.bld=var([1, 0], [0, 64, 0, 64, 64, 0, 0, 128, 0, 64, 64, 0, 0, 128, 0, 64])
var.drp=var([1, 0], [0, 64, 0, 64, 0, 64, 128, 0, 0, 64, 0, 64, 128, 0, 0, 64])
var.out=var([1, 0], [0, 64, 0, 64, 0, 64, 0, 128, 0, 64, 0, 64, 0, 128, 64, 0])
var.brks=var([1, 0], [28, 4, 30, 2, 14, 2, 24, 8, 12, 4])
#SYNTHZ
s1 >> prayerbell(note3, oct=4, dur=8, sus=4, echo=1/2, verb=1/4, room=3/5, mix=1/2, pan=PWhite(-1, 1), amplify=2/16, amp=1*(var.int+var.brk+var.bld+var.drp+var.out))
s2 >> snick(note, dur=var([PDur(3, 8), 1/2], [12, 4]), sus=1/2, verb=3/5, room=3/5, mix=1/3, pan=PRand([-1/2, 1/2]), amplify=6/5, amp=1*(var.out))
s3 >> donk(note, oct=5, dur=PDur(3, 8), sus=3, formant=linvar([0, 3], [4, 0]), verb=3/8, room=2/5, mix=1/2, amplify=4/5, amp=1*(var.brk+var.bld+var.drp))
s4 >> soprano((note+[-2, 2], note), oct=var([2, 3]), dur=4, sus=4, shape=1/3, verb=2/5, room=2/5, mix=1/2, amplify=3/7, amp=1*(var.brk+var.bld+var.drp))
s5 >> soprano((note, note3), oct=var([4, 5]), dur=PRand(3, 8), verb=2/5, room=2/5, mix=1/2, amplify=3/7, amp=1*(var.brk+var.bld+var.drp))
s6 >> pads(note3, oct=3, dur=8, sus=8, drive=1/7, pan=linvar([-1, 1], 16), amplify=4/5, amp=1*(var.bld+var.drp))
s7 >> pluck(note2, oct=var([3, 4], [6, 2]), dur=var([PDur(5, 8), 1/2], [6, 2]), sus=2, formant=4, room=2/3, mix=1/3, pan=PWhite(-1, 1), amplify=3/7, amp=1*var.drp)
s8 >> space(note2, oct=4, dur=4, sus=2, verb=2/5, room=2/5, mix=1/2, amplify=4/5, amp=1*(var.bld+var.drp+var.out)).offbeat()
s9 >> donorgan(note, oct=PRand([5, 6]), dur=[4, 2], echo=[0, 1/2], room=expvar([1/3, 2/3], 32), mix=1/2, amplify=3/5, amp=1*(var.bld+var.drp))
s0 >> shore(dur=PRand([1, 2, 4, 8], 8), lpf=PxRand(200, 3800), room=2/3, mix=1/2, pan=PWhite(-1, 1), amplify=3/5, amp=1*(var.int+var.brk+var.out))
t1 >> click(note, oct=PRand([4, 5]), dur=var([PDur(3, 8), 2], [6, 2]), sus=4, room=2/5, mix=1/3, amplify=2/6, amp=1*(var.drp))
#BEATZ
b1 >> play("([VV]V)....V.(..V)", dur=1/2, rate=1, sample=2, amplify=1, amp=1*(var.bld+var.drp))
b2 >> play("w", dur=1/4, delay=PShuf([0, 1/2, 0, 0, 1/2]), rate=3, lpf=sinvar([200, 900], 16), room=3/5, mix=1/3, amplify=3/5, amp=1*(var.bld+var.drp+var.out))
b3 >> play("S", rate=PRand([1, 7/5, 6/5], PRand([1/2, 1, 2, 4])), sample=2, amplify=3/8, amp=1*(var.brk+var.bld+var.drp)).sometimes("stutter", 3).every(8, "stutter", 2)
b4 >> play("..t{(.)([.t])}", dur=1, sample=0, room=1/3, mix=1/2, amplify=3/2, amp=1*(var.bld+var.drp)).sometimes("stutter", 2)
b5 >> play("..o.", dur=1/2, sample=4, room=1/3, mix=1/3, amplify=4/5, amp=1*var.drp)
b6 >> play("f", dur=1/2, delay=PShuf([0, 1/2, 3/4, 0, 3/8, 1/2, 0, 5/8, 0, 1/4]), rate=2, sample=1, room=3/4, mix=1/3, amplify=PRand([0, 1/3], 4), amp=1*(var.bld+var.drp))
b7 >> play("[--]", sample=0, amplify=1/2, amp=1*(var.bld+var.drp))
b8 >> play("~", sample=4, verb=4/5, room=2/5, mix=1/2, amplify=4/5, amp=1*var.drp)
b9 >> play(".s", dur=1/2, sample=4, amplify=4/5, amp=1*(var.brk+var.bld+var.drp)).sometimes("stutter", 3).every(16, "stutter", 2)

#Show synths available
print(SynthDefs)
#Show synth attributes
print(Player.get_attributes())
#Show available Scales
print(Scale.names())
