#Example Ambient 
Clock.set_time(0)

#TEMPO
Clock.bpm=70
#ROOT 
Root.default.set(-2)
#SCALE
Scale.default = Scale.chromatic
#NOTES
note = P[(0, 12)]
dmtf2 = [6, 2, 4, 2, 2, 4, 3, 1, 4, 4]
dmtf3 = [6, 2, 4, 4, 4, 4, 3, 1, 4, 3, 1, 4, 3, 1, 2, 2]
dmtf4 = [5, 3, 4, 8, 4, 4, 3, 4, 1, 4, 4, 4]
dmtf6 = [dmtf4[7], dmtf4[8], dmtf4[9], dmtf4[10], dmtf4[11]]
dmtf7 = [3, 1, 4, 3, 1, 4, 2, 2, 4, 2, 2, 4]
var.mtf = var([7, 4, 5, 7, 12, 7], 4)
var.mtf2 = var([12, 7, 10, 9, 5, 7, 0, 2, 4, 7], dmtf2)
var.mtf3 = var([6, 4, 3, -1, 3, 4, 1, -1, -3, 0, 2, 3, 8, 9, 8, 3], dmtf3)
var.mtf4 = var([11, 9, 11, 9, 11, 9, 11, 9, 7, 11, 10, 12], dmtf4)
var.mtf5 = var([-5, 0, -5, 0, -5, 0, -5, 0, -5, 0, -5, 0, -5, 0, 4, 0, -5, 0, -5, 0, -5, 0, -5, 0, -5, 0, -5, 0, -5, 0, 4, 0, 7, -5, 4, -5, 0, -5, 4, -5, 7, -5, 4, -5, 0, -5, 0, -5, 7, -5, 4, -5, 0, -5, 0, -5, 7, -5, 4, -5, 0, -5, 0, -5], 1/2)
var.mtf6 = var([var.mtf[7], var.mtf[8], var.mtf[9], var.mtf[10], var.mtf[11]], dmtf6)
var.mtf7 = var([-2, -7, 2, 5, 2, 7, -2, 10, 12, 9, 22, 24], dmtf7)
var.chrds = var([(0, -5), (0, -5), (0, 4), (0, 5), (0, 7), (0, 7), (0, 12), (0, 7)], 4)
var.chrds2 = var([(0, 7), (0, 4), (4, 12), (4, 7)], 8)
var.chrds3 = var([(-1, 6), (-1, 3), (3, 6), (3, 11)], 4)
var.chrds4 = var([(-2, 5), (-2, 3), (3, 8), (8, 12)], 4)
var.chrds5 = var([(8, 12 ), (8, 15), (8, 20), (8, 15)], 4) 
var.chrds6 = var([(-2, 5, 14), (-2, 5, 17), (-2, 5, 14), (-2, 5, 17), (0, 7, 16), (0, 7, 19), (0, 7, 19), (0, 7, 19)], 4)
var.chrds7 = var([(-2, 2), (-2, 2, 5), (-2, 2), (-2, 2, 5), (0, 4), (0, 4, 7), (0, 4, 7), (0, 4, 7)])
#SYNTHS
s1 >> subbass2(note+var([0, -1, -4, -5, 0, -5, -2, 0], [32, 16, 16, 16, 32, 16, 16, 16]), oct=3, dur=16, atk=2, room=3/4, mix=4/5, amplify=3/8, amp=1)
s2 >> filthysaw(note+var([0, -1, -4, -5, 0, -5, -2, 0], [32, 16, 16, 16, 32, 16, 16, 16]), oct=4, dur=16, atk=4, rel=2, hpf=expvar([500,600],16), hpr=1/4, room=3/4, mix=1/2, amplify=3/10, amp=1)
s3 >> noise(note+var([0, -1, -4, -5, 0, -5, -2, 0], [32, 16, 16, 16, 32, 16, 16, 16]), oct=3, dur=16, atk=4, hpf=1800, hpr=1/4, lpf=2400, lpr=1/2, formant=1, room=2/3, mix=1/2, pan=(-2/3, 2/3), amplify=2/9, amp=1)
s4 >> rsin(note+var([0, -1, -4, -5, 0, -5, -2, 0], [32, 16, 16, 16, 32, 16, 16, 16]), oct=4, dur=8, atk=4, lpf=900, lpr=1/2, amplify=2/20, amp=1)
s5 >> flute(var([var.mtf, var.mtf3, var.mtf], [32, 48, 80]), oct=5, atk=2, dur=var([4, dmtf3, 4], [32, 48, 80]), sus=6, room=2/3, mix=3/4, amplify=linvar([3/9, 4/9], 16)*var([1, 0],[32, 0, 48, 0, 0, 80]), amp=1)
s6 >> strings(s5.degree, oct=5, dur=s5.dur, atk=1, sus=8, shape=1/8, lpf=expvar([800, 1600], 16), room=2/3, mix=1/2, amplify=3/8*var([0, 1],[32, 0, 48, 0, 0, 80]), amp=1)
s7 >> noisynth(var([var.chrds, var.chrds3, var.chrds4, var.chrds5, var.chrds], [32, 16, 16, 16, 80]), oct=5, dur=4, atk=2, sus=6, room=2/3, mix=3/4, amplify=var([3/7, 3/7, 3/7, 0],[32, 48, 80]), amp=1)
s8 >> borgan(var([var.chrds2, var.chrds3, var.chrds4, var.chrds5, var.chrds2], [32, 16, 16, 16, 80]), oct=5, dur=var([8, 4, 4, 4, 8], [32, 16, 16, 16, 80]), atk=1, lpf=900, lpr=1/2, room=3/4, mix=1/2, pan=(-4/5, 4/5), amplify=var([5/8, 0], [80, 80]), amp=1)
s9 >> glass(var([var.mtf2, var.mtf4, var.mtf2, var.mtf6, var.mtf2], [32, 48, 32, 16, 32]), oct=6, dur=var([dmtf2, dmtf4, dmtf2, dmtf6, dmtf2], [32, 48, 32, 16, 32]), shape=1/8, room=1/3, mix=var([1/3,4/5],16), pan=(-2/3,2/3), amplify=var([5/8,0], [128, 32]), amp=1)
t1 >> mhping(var.mtf5, oct=8, dur=1/2, sus=1/2, hpf=1200, swell=1, room=5/6, mix=3/4, amplify=var([0, 4/12, 0, 0, 0], [80, 32, 16,  16, 16]), amp=1)
t2 >> rhodes(note + var([0, 0, -5, -2, 0]), oct=(3, 4), dur=8, atk=3, sus=12, echo=(1/2, 3/4), echoTime=2, lpf=2800, lpr=2/3, fmod=1, room=2/3, mix=2/3, pan=(-2/3, 2/3), amplify=var([0, 3/12, 3/12, 3/12, 3/12], [80, 32, 16, 16, 16]), amp=1)
t3 >> viola(var.mtf, oct=(3,7), dur=4, atk=1, sus=4, shape=1/8, lpf=2400, lpr=2/3, room=4/5, mix=1/2, amplify=var([0, 3/8, 0, 0, 0, 0], [80, 32, 16, 16, 16]), amp=1)
t4 >> varsaw(var.chrds6, oct=5, dur=4, atk=1, sus=6, lpf=3800, room=4/5, mix=1/2, amplify=var([0, 3/8], [128, 32]))
t5 >> sillyvoice(var.chrds7, oct=(4, 5), atk=1/3, dur=4, swell=1/8, vib=0, tremolo=(0, 2), fmod=1, lpf=2400, lpr=1/2, room=5/6, mix=3/5, amplify=var([0, 3/8], [128, 32]), amp=1)
t6 >> cs80lead(var.chrds7, oct=7, dur=4, sus=6, formant=2, tremolo=(0, 2), room=5/6, mix=4/5, pan=(-2/3, 2/3), amplify=var([0, 3/8], [128, 32]), amp=1)
t7 >> spark(var.mtf7, oct=5, dur=dmtf7, sus=3, hpf=700, hpr=3/4, lpf=1800, lpr=1/2, room=3/4, mix=1/2, amplify=var([0, 3/8], [128, 32]), amp=1)
t8 >> garfield(var.mtf7, oct=6, dur=dmtf7, lpf=900, room=2/3, mix=2/3, amplify=var([0, 3/8], [128, 32]), amp=1)
t9 >> pads(var.mtf7, oct=6, dur=dmtf7, sus=4, room=3/4, mix=1/2, amplify=var([0, 2/8], [128, 32]), amp=1)

print(SynthDefs)

print(Attributes)
