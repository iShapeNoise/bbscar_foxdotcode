#Example_FutureChill
Clock.set_time(0)

#TEMPO
Clock.bpm = 130
#Scale
Scale.default = Scale.lydian
#NOTES
var.bl = Pvar([1, -1], 16)
var.chrds = var([(1, 3, 5), (-1, 1, 3)], 16)
mtf = [1, 2+1/2, 3, 4, 5]
mtf2 = [-1, 0, 1, 3, 4]
mtf3 = P[1, 3, 1, 5, 1, 3, 1, [0, -1]]
mtf4 = P[-1, 1, -1, 3, -1, 1, -1, [-3, 3]]
mtf5 = P[1, 1, 1/2, 1, 1/2, 1, 1, 1, 1/2, 1, 1/2, 1]
mtf6 = P[-1, -1, -1.5, 1, -1.5, -1, -1, -1.5, -1, -1.5, -1]
#TIMING
var.intro = var([1, 0], [64, 0, 0, 128, 0, 128, 0, 64])
var.sect1 = var([1, 0], [0, 64, 128, 0, 0, 128, 0, 64])
var.sect2 = var([1, 0], [0, 64, 0, 128, 128, 0, 0, 64])
var.outro = var([1, 0], [0, 64, 0, 128, 0, 128, 64, 0])
#SYNTHS
s1 >> subbass(var.bl, oct=4, dur=16, lpf=800, fmod=1, room=3/4, mix=1/2, amplify=7/8, amp=1*(var.intro+var.sect1+var.sect2))
s2 >> spark(var.bl, oct=4, atk=2, dur=16, echo=1/4, lpf=1600, fmod=1, room=3/4, mix=1/2, amplify=5/8, amp=1*(var.intro+var.sect1+var.sect2+var.outro))
s3 >> filthysaw(var.bl, oct=6, dur=4, lpf=900, glide=var([0, 0, 0, 4, 0, 0, 0, -2]), fmod=1, room=3/4, mix=var([1/2, 3/4], [12, 4]), pan=(-2/3, 2/3), amplify=5/8, amp=1*(var.sect1+var.sect2))
s4 >> donorgan(var.chrds, oct=5, dur=4, atk=1/2, delay=2, sus=2, hpf=linvar([1200, 2400], 16), formant=1/2, room=3/4, mix=3/4, amplify=5/6, amp=1*var.sect1)
s5 >> tworgan4(var.chrds, oct=5, atk=1/2, dur=4, sus=4, glide=var([0, 0, 0, 4, 0, 0, 0, -2], 4), hpf=900, room=1/2, mix=1/2, pan=(-2/3, 2/3), amplify=4/8, amp=1*var.sect2)
s6 >> strings(Pvar([mtf5, mtf6], 16), oct=var([5, 6], 16), dur=var([1, PDur(5, 8)], [1, 3]), room=1/2, mix=2/3, amplify=var([0, 4/8], [1, 3]) * var([1, 0], 32), amp=1*var.sect2)
s7 >> karp([var.bl, Pvar([PRand(mtf, seed=9), PRand(mtf2, seed=13)], 16)], oct=var([6, 7], 16), dur=var([1, PDur(5, 8)], [6, 2]), room=3/4, mix=1/2, amplify=expvar([4/6, 6/7], 32), amp=1*(var.sect1+var.outro))
s8 >> tribell(Pvar([mtf3, mtf4], 16), oct=6, dur=Pvar([[1, 1/2, 1/4, 1/4], 2/3], [12, 4]), sus=2, fmod=1, formant=0, room=1/3, mix=1/3, amplify=expvar([6/8, 8/8], [32, 16])*var([0, 1], 64), amp=1*var.sect2)
s9 >> hoover(s8.degree, oct=5, dur=4, chop=4, fmod=1, shape=1/3, lpf=1200, hpr=3/4, glide=[0, 0, 0, 4, 0, 0, 0, -2], room=1/3, mix=1/2, amplify=linvar([4/12, 7/12], 16), amp=1*(var.sect1+var.sect2))
s0 >> square(Pvar([var.bl, Pvar([mtf3, mtf4], 16)], [1/2, 5/2]), oct=3, dur=Pvar([1/2, PDur(5, 8)], [3, 1]), sus=1/4, formant=var([5, 4], 16), lpf=linvar([800, 1800], 16), drive=1/12, room=3/4, mix=1/2, amplify=var([0, 1], 32) * 5/7, amp=1*(var.intro+var.sect1))
#SAMPLES
b1 >> play("V", dur=4, delay=[0, (1/20, 3/2), 0, (1/20, 3/2)], sample=26, room=1/2, mix=1/3, amplify=4/5, amp=1*(var.sect1+var.sect2))
b2 >> play("V", dur=4, delay=[0, (1/20, 3/2), 0, (1/20, 3/2)], sample=8, amplify=4/5, amp=1*(var.sect1+var.sect2))
b3 >> play("r", dur=2, delay=[0, (0, 1/2), 0, (0, 3/4)], sample=5, rate=1, pshift=var([-2, 3], 16), room=1/2, mix=1/2, amplify=var([2/4, 1/4], [1, 3])*var([0, 1], 32), amp=1*(var.intro+var.sect1+var.sect2))
b4 >> play("t", dur=4, delay=2 , sample=18, rate=6/5, shape=2/3, amplify=1, amp=1*(var.sect1+var.sect2))
b5 >> play("u", dur=4, delay=2, sample=10, rate=6/5, shape=2/3, echo=1/8, echoTime=1, coarse=4, fmod=0, hpf=400, room=2/3, mix=1/2, amplify=3/9, amp=1*(var.sect1+var.sect2))
b6 >> play("s", dur=1, delay=1/2, cut=1/4, sample=15, rate=1, lpf=1200, shape=2/3, room=3/4, pan=(-3/4, 3/4), mix=1/2, amplify=3/5, amp=1*(var.sect1+var.sect2)).every(8, "stutter", 2, cut=1/2)
b7 >> play("s", dur=1, delay=1/2, sample=1, amplify=4/5, amp=1*(var.sect1+var.sect2*var.outro))
b8 >> play("S", dur=1/2, sample=3, rate=[1, 3/4, 6/5, 4/5], room=1/3, mix=1/2, amplify=[3/7, 1/3], amp=1*(var.sect1+var.sect2))
b9 >> play("C", dur=2, pshift=var([-2, 1, 3, -1], [14, 2]), room=2/3, mix=3/4, amplify=var([3/5, 0], 32), amp=1*(var.sect1+var.sect2))
