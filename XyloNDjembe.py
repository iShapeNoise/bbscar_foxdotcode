#XyloNDjembe (FoxDot Branch killa_features are necessary)
Clock.set_time(0)

Clock.bpm=90
#SCALE
Scale.default=Scale.mixolydian
#MELODIC
var.mtf1 = var([0, 1, 0, 2], PRand([32, 64], seed=2))
var.mtf2 = var([PRand([0, 1, 2, 4], seed=13)], PRand([32, 64, 128],seed=19))
var.mtf3 = var([PRand([Scale.default],seed=5)], PRand([PDur(3,5), 1/2, 1, 2, 4, 8, 16], PRand([16, 32, 64], seed=11)))
#RHYTHMIC
dmtf1 = PDur([3, 5], 8)
dmtf2 = P[1/2, 1/4, 1/4, 1/4, 1/4, 1/2]
dmtf3 = P[1/2, 1/4, 1/4, PDur(3, 5), PSum(7, 2)]
dmtf4 = P[1, 3/2, 1/2, 1/4, 1/4, 1/2]
dcon = P[1/2, 1/4, 1/4]
var.brks = var([1, 0],[28, 4, 64, 0, 56, 8, 128, 0, 64, 0, 54, 8, 28, 4])
#SYNTHS
s1 >> xylophone(var.mtf1, oct=3, dur=PRand([PRand([1, 2]), dmtf1], 64), spin=linvar([0, 1/2]), pan=(-2/3, 2/3), room=2/3, mix=1/2, amplify=var([4/6, 2/3], [1, 3]), amp=1)
s2 >> xylophone(var.mtf2, oct=4, dur=Pvar([PShuf(dmtf2), PwRand([dmtf1, dmtf2], [12, 4])]), room=2/3, mix=1/2, pan=(-2/3, 2/3), amplify=var([1/2, 3/7], [1/2, 3/2]), amp=1)
s3 >> xylophone(var.mtf3, oct=5, dur=dmtf3 | P[dmtf3 + dmtf4] | P[dmtf3 + dmtf4] | dmtf4, room=4/5, mix=1/2, pan=PWhite(-1, 1), amplify=PwRand([3/7, 1/3], [8, 1])*PRand([0, 1],[32, 64, 128], seed=21), amp=1)
s4 >> kalimba(var.mtf3, oct=6, dur=8, echo=1/2, echotime=4, bend=[0, 1/12], benddelay=1/4, room=4/5, mix=1/2, amplify=PRand([0, 1/8])*PRand([1, 0],[16, 32, 64, 128], seed=19), amp=1)
#SAMPLES
b1 >> play("V", dur=4, delay=0, sample=12, amplify=2/5*var.brks, amp=1)
b2 >> play("X", dur=2, delay=[0, [0, 1/2]], sample=12, amplify=PRand([0, [2/5, 1/5, 2/5, 1/5]], [16, 32, 64, 128])*var.brks, amp=1)
b3 >> play("A", dur=2, delay=[(0, 1/4), 0, 0, [0, 1/2], 0, 0], sample=12, amplify=PRand([0, [2/5, 1/5, 2/5, 1/5]],[128, 32, 64])*var.brks, amp=1)
b4 >> play("O", dur=2, delay=1, sample=12, room=2/3, mix=1/3, amplify=PRand([0, [2/5, 1/5, 2/5, 1/5]], [128, 32, 4])*var.brks, amp=1).sometimes("stutter", degree="o", delay=(0, 1/4), amplify=2/7)
b5 >> play("{iI}", dur=2, delay=[1/2, 1/2, PRand([1/2, 1/4]), 1/2], sample=12, room=2/3, mix=1/3, amplify=var([0, 3/8],PRand([16, 32, 64, 128]))*var.brks, amp=1)
b6 >> play("{mM}", dur=1, delay=[0, 0, (0, 1/4), 0, 0, 1/2, 0, (0, 1/4)], sample=12, amplify=var([0, 3/7], PRand([16, 32, 64, 128], seed=12))*var.brks, amp=1)
b7 >> play("{wpP}", dur=3, delay=1/2, sample=12, room=2/3, mix=1/3, amplify=var([0, 1/4], PRand([16, 32, 64, 128], seed=9))*var.brks, amp=1)
b8 >> play("r", dur=1/2, delay=1/2, sample=12, rate=1, room=2/3, mix=1/2, amplify=Pvar([0, var([1/2, 1/4], [1/2, 9/10])], PRand([16, 32, 64, 128],seed=8))*var.brks, amp=1).sometimes("stutter", 3).every(32,"stutter", 3)
b9 >> play("R", dur=1, sample=12, room=2/3, mix=1/2, amplify=var([0, 1/3], PRand([32, 64, 128], seed=6)), amp=1).every(16, "stutter", 1, delay=(0, 1/4))
b0 >> play("f", dur=PRand([16, 32, 64, 128]), delay=b0.dur-1, rate=4/5, sample=12, amplify=4/5*var.brks, amp=1)
c1 >> play("D", dur=1, delay=1/2, sample=12, room=2/3, mix=1/2, amplify=var([0, 1/4], PRand([32, 64, 128], seed=3))*var.brks, amp=1).sometimes("stutter", 2, amplify=1/4).every(16, "amen")
c2 >> play("T", dur=PRand([2, PDur(3, 5), 1], [32, 64, 96]), delay=var([0, 1/2], PRand([16, 32, 64])), sample=12, room=2/3, mix=1/2, amplify=var([0, 1/3], PRand([16, 32, 64, 128], seed=15))*var.brks, amp=1).sometimes("stutter", 2, delay=1/4)
c3 >> play("y", dur=PRand([32, 64]), delay=0, rate=6/5, sample=12, room=2/3, mix=1/2, amplify=PRand([0, 1/3], [32, 64, 128])*var.brks, amp=1)
