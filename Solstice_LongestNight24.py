###### HELLO TO THE DARKEST >> LONGEST NIGHT OF NIGHTS IN A CIRCLE #########
###### GLAD TO BE HERE ANOTHER YEAR >> LERIG IS HERE #######################
############# WITH >> BORNHOLMER STRASSE << ################################

# Tempo
Clock.bpm=128
# Sample path
path = "/home/LeRig/Desktop/Sets/NxtSet/estairs/"
# Root
var.root = var([0,1], 128)
# Scale
Scale.default = Scale.mixolydian
# Timing
var.brks = var([1,0], [14,2,30,2,14,2,32,0,24,8])
# Intro Dur
var.dur=1

# Subway station
f1 >> play("k", sdb=1, sample=22, dur=64, room=1, mix=1/2, pan=(-2/3,2/3), amplify=3/2, amp=0)
f1.amp=Pvar([linvar([1,1/8], [1024,0]),1/8,0], [1024,512,inf], start=nextbar) 

# Subway
f2 >> play("k", sdb=1, sample=23, dur=32, rate=[1,-1], room=1, mix=1/2, amplify=6/5*var([1,0,0,1]), amp=0)
f2.amp=var([1,0], [128,inf], start=nextbar)

l1 >> loop(path+"estairs_2.wav", dur=16, beat_stretch=True, pshift=var.pitch, lpf=400, lpr=1/4, room=1/2, mix=1/3, amplify=3/2, amp=1)
l2 >> loop(path+"estairs_2.wav", dur=16, beat_stretch=True, pshift=var.pitch, hpf=900, hpr=1/2, room=4/5, mix=1/2, amplify=1, amp=1)
l3 >> loop(path+"estairs_2.wav", dur=16, beat_stretch=True, pshift=var.pitch, chop=32, room=1/2, mix=1/3, amplify=1, amp=1)
estairs1 = Group(l1,l2,l3)
estairs1.amp=Pvar([linvar([0,1], [128,0]),1], [128,inf], start=nextbar) 

# Random note stepper with root note priority
steps=8
scale = list(Scale.default)
var.note = random.choices(scale, (50,10,10,10,10,10,10), k=steps)
var.bl = var([var.note[0], var.note[3]], 4)
var.seq = var(var.note,var.dur)
print(var.note)
print(var.bl)

# Random velocity values
velvals = random.sample(range(1,100), steps)
var.velo = var([P[velvals]/100], var.dur)
print(P[velvals]/100)

# Randomly trigger notes
tstep = [0,1]
tsteps = random.choices(tstep,(10,20),k=steps)
var.trig = var(tsteps, var.dur)
print(tsteps)

l4 >> play("Z", sdb=1, sample=15, dur=8, chop=16, pshift=var.seq-1, room=2/3, mix=1/2, lpf=linvar([400,4000], 64), lpr=1/4, pan=(-2/3,2/3), amplify=4/5, amp=1)
l5 >> play("Z", sdb=1, sample=18, dur=8, chop=16, pshift=var.seq-1, room=2/3, mix=1/2, lpf=linvar([400,4000], 64), lpr=1/4, pan=(-2/3,2/3), amplify=3/5, amp=1)
ambi = Group(l4,l5)
ambi.amp=1

# Dong
p1 >> donk(var.bl, oct=3, dur=16, room=1/2, mix=1/3, amplify=6/5*P[(6/5,2/3)], amp=0)
p1.amp=1

estairs1.amp=0
p2 >> xylophone(var.bl, oct=4, dur=PDur(3,8), room=3/4, mix=4/5, amplify=8/5*var.velo, amp=0)
p3 >> xylophone(var.seq, oct=4, dur=PDur(3,8), room=3/4, mix=4/5, amplify=8/5*var.velo, amp=0)
p4 >> xylophone(Pvar([P[:2],P[:4]], 32), oct=var([5,6],[15/2,1/2]), dur=1/4, room=3/4, mix=4/5, amplify=1/2, amp=0)
s_xylo = Group(p2,p3,p4)
s_xylo.amp=1

l6 >> loop(path+"speechbeat_1.wav", dur=32, sus=30, beat_stretch=False, room=2/3, mix=1/2, amplify=PxRand([3/4,1]), amp=1)
speaker = l6
speaker.amp=var([1,0], [32,inf], start=nextbar)

ambi.amp=0

p1.amp=0
k1 >> play("v", sdb=1, sample=6, dur=8, delay=0, echo=[0,1/2], pan=0, amplify=6/5*var.brks, amp=1)
k2 >> play("V", sdb=1, sample=8, dur=2, delay=[0,(0,3/2)], pan=0, amplify=3/4*var.brks, amp=1)
bkick1 = Group(k1,k2)
bkick1.amp=1

p5 >> flute(var.seq, oct=5, dur=1/3, blur=3/2, room=3/4, mix=linvar([1/2,3/4], 16), pan=PWhite(-3/4,3/4), chop=[0,0,0,[1,2]], amplify=2*PxRand([1,4], seed=7)/4*linvar([2/3,1], PRand([32,64,96], seed=43)), amp=0)
p5.amp=1

s_xylo.amp=0
l7 >> loop(path+"estairs_1.wav", dur=8, beat_stretch=True, pshift=var.pitch, pan=(-2/3,2/3), room=2/3, mix=1/2, amplify=1, amp=1)
l8 >> loop(path+"estairs_1.wav", dur=8, beat_stretch=True, pshift=var.pitch, hpf=900, hpr=1/4, room=1/2, mix=1/2, amplify=4/5, amp=1)
l9 >> loop(path+"estairs_1.wav", dur=8, beat_stretch=True, pshift=var.pitch, chop=64, room=1/2, mix=1/2, amplify=4/5, amp=1)
estairs2 = Group(l7,l8,l9)
estairs2.amp=1

estairs2.amp = Pvar([linvar([1,0], [128,0]),0], [128,inf], start=nextbar) 
p6 >> glass((var.seq, var.seq+1), oct=4, dur=16, lpf=linvar([2400,500], 128), lpr=1/2, beat_dur=1, peak=4/5, level=6/5, room=1, mix=1/2, amplify=4/5, amp=0)
p6.amp=1

p7 >> pads(var.seq, oct=(3,4), dur=16, sus=24, blend=1, chop=32, lpf=1200, shape=1/8, room=1, mix=3/4, amplify=4/5, amp=0)
p7.amp=Pvar([linvar([0,1], [64,0]),1], [64,inf], start=nextbar)

bkick1.amp=0

p8 >> tworgan(var.bl, oct=3, dur=4, atk=1, blur=3/2, tremolo=2, phaser=1/4, phaserdepth=2/3, room=3/4, mix=2/3, amplify=2/3, amp=1)
p8.amp=Pvar([linvar([0,1], [64,0]),1], [64,inf], start=nextbar)

p5.amp=0
p9 >> piano(var.bl, oct=4, dur=1, sus=1, room=linvar([2/3,4/5], 64), mix=1/2, amplify=6/7*var.trig, amp=0)
p0 >> piano(var.seq, oct=5, dur=var([1/4,2/3], [16,32]), sus=p0.dur*2, room=linvar([2/3,4/5], 64), mix=1/2, amplify=6/5*var.velo, amp=0)
s_piano=Group(p9,p0)
s_piano.amp=4/5

s_atmo = Group(p6,p7,p8)
s_atmo.amp = Pvar([linvar([1,0],[128,0]),0], [128,inf], start=nextbar)

v1 >> play("#", dur=4, sdb=1, sample=1, rate=7/8, verb=3/4, lpf=1800, room=4/5, mix=4/5, pan=(-2/3,2/3), amplify=2, amp=0)
v1.amp=var([1,0], [4,inf], start=nextbar)

pa >> swell(var.seq, oct=4, dur=[64,48,96], delay=PRand([0,8,16,24,32], seed=13), atk=3, blur=2, lpf=2400, lpr=1/3, room=2/3, mix=2/3, amplify=3/9, amp=1)
pa.amp=Pvar([linvar([0,1], [32,0]),1], [32,inf], start=nextbar)

pb >> ssaw(var.bl, oct=3, dur=16, sus=32, delay=PRand([4,8,16,24,12], seed=9), atk=2, shape=1, blur=3/2, lpf=200, lpr=1/2, room=2/3, mix=2/3, amplify=4/5, amp=0)
pb.amp=1

pc >> longsaw(var.bl, oct=3, dur=8, chop=16, bend=[1/20,0,-1/20,0], lpf=800, room=4/5, mix=3/4, amplify=4/5*var.velo*var.trig, amp=0)
pc.amp=1

s_piano.amp=Pvar([linvar([1,0],[128,0]),0], [128,inf], start=nextbar)
pd >> space(var.bl, oct=3, dur=4, sus=2, delay=1, chop=[0,8,0,4], formant=2, room=4/5, mix=2/3, amplify=6/5, amp=0)
pd.amp=1

v2 >> play("2", sdb=1, sample=4, dur=4, room=4/5, mix=3/4, pan=(-3/4,3/4), amplify=5/6, amp=0)
v2.amp=var([1,0], [4,inf], start=nextbar)
v5 >> play("2", sdb=1, sample=8, dur=4, shape=1/2, hpf=900, hpr=1/4, room=4/5, mix=3/4, amplify=1/2, amp=0)
v5.amp=var([1,0], [4,inf], start=nextbar)

v3 >> play("2", sdb=1, sample=5, dur=4, room=7/8, mix=3/4, pan=(-3/4,3/4), amplify=4/7, amp=0)
v3.amp=var([1,0], [4,inf], start=nextbar)
# Wusch
v4 >> play("2", sdb=1, sample=6, dur=16, room=1, mix=3/4, amplify=1/2, amp=0)
v4.amp=var([1,0], [16,inf], start=nextbar)

p3.amp=1
t1 >> play("m", sdb=1, dur=2, delay=[0,1/2], sample=PRand([4,5,6], PRand([32,64,96], seed=13), seed=12), room=3/4, mix=1/3, amplify=4/5*var.brks, amp=1).rarely("stutter", sample=9, amplify=1/2, amp=0)
t1.amp=1

t2 >> play("d", sdb=1, dur=16, delay=5, sample=4, echo=[0,1/2], room=1, mix=1/2, amplify=1/2*var([0,1])*var.brks, amp=0).rarely("stutter", echo=(1/2, 2/3))
t2.amp=1

pe >> sinepad(var.bl, oct=3, dur=1, sus=sd.dur*2, tremolo=3, formant=1, room=linvar([1/2,2/3], 64), mix=3/4, amplify=4/5*var.velo, amp=0)
pe.amp=1

s_atmo2 = Group(pa,pb,pc)
s_atmo2.amp=Pvar([linvar([1,0],[128,0]), 0], [128,inf], start=nextbar)
pf >> bass(var.bl, oct=4, dur=1, blend=3/2, chop=[2,3,2,4], shape=Pvar([1/2,linvar([0,2], [8,0])], [96,32]), room=2/3, mix=1/3, amplify=3/2, amp=0)
pf.amp=1

bkick1.amp=1

s3 >> play("O", sdb=1, sample=10, dur=4, delay=2, cut=1/2, room=var([1/2,3/4], [12,4]), mix=var([1/2,3/4], [12,4]), amplify=5/4*var.brks, amp=1)
snare1=s3
snare1.amp=1

h1 >> play("-", sdb=1, sample=6, dur=1, delay=1/2, echo=[0,1/2], room=var([1/2,2/3], [12,4]), mix=var([1/2,3/4], [12,4]), amplify=7/6*var.brks, amp=1).every(16, "stutter", 2).solo(0)
h2 >> play("S", sdb=1, sample=22, dur=var([1,PDur(3,5)], [6,2]), delay=0, echo=[0,1/2], amplify=6/5, amp=1)
hihat1 = Group(h1,h2)
hihat1.amp=1

ambi.amp=1

Master().elpf=Pvar([linvar([0,2000], [256,0]),2000], [256,inf], start=nextbar)
Master().room=Pvar([linvar([1/2,1], [256,0]),1], [256,inf], start=nextbar)
Master().mix=Pvar([linvar([1/4,3/4], [256,0]),3/4], [256,inf], start=nextbar)







