Clock.reset()
Clock.set_time(0)
Clock.bpm = 150
s = '/home/bbscar/Sound/SAMPLES/KittyClock/green/'

var.hpf = var([0, 500], [31, 1, 56, 8, 32, 0, 28, 4, 32, 0])

a1 >> ppad(dur=4, oct=3, hpf=P[150, 120, 200, 170, 100], amp=1)
a2 >> filthysaw(dur=var([2, 4], 128), tremolo=2, oct=3, cf=linvar([500, 200], 64), lpf=400, hpf=var.hpf, amplify=2/3, amp=1)
gA = Group(a1, a2)
gA.amp=0

gA.amp=linvar([0, 1, 1], [32, 0, inf], start=Clock.now())


b1 >> play('S', sample=PRand(40), dur=PWhite(2, 7), rate=PWhite(-0.25, 0.25), room=PWhite(0, 1), mix=PWhite(0.01, 0.5), pan=PWhite(-0.5, 0.5), amplify=PWhite(0, 1) * 0.6, amp=1)
b2 >> rsaw(oct=3, dur=PDur(3, 5), rel=expvar([2, 1.2], [16, 64]), hpf=expvar([150, 100], 16), lofreq=expvar([900, 400], [32, 16, 64]), hifreq=expvar([1000, 8000], 16), slide=[0, 0, 0, 1/2], formant=3, room=1/2, mix=1/3, pan=PSine(32), amplify=var([0.3, 0.65], 32), amp=1)
b3 >> eeri(blur=2, level=0.5, cutoff=expvar([200, 2200], 16), dur=2, pan=PWhite(-0.5, 0.5), hpf=var.hpf, amplify=3/9 * expvar([0, 1], [128, 64, 32, 16, 8]), amp=1)
b4 >> rlead(P[:-2], oct=6, dur=2, sus=2, delay=1, tremolo=4, hpf=linvar([2000, 12000], 32), hpr=0.75, formant=1, room = 4/5, mix=0.5 * linvar([1/2, 1], [[16, 32, 64], 0]), amplify=1, amp=1)
gB = Group(b1, b2, b3, b4)
gB.amp = 0

gB.amp=linvar([0, 1, 1], [16, 0, inf], start=Clock.now())


c1 >> play('o', sample=8, dur=1, room=1, mix=0.2, amplify=P[0, [expvar([0, 1, 1], 128), 1]] * 3/4, amp=1)
c2 >> play('x', dur=0.5, hpf=var.hpf, amplify=expvar([1, [0.25, 0.5]], [1, 0]) * 1.35 * 4/5, amp=1)
c3 >> play('w', rate=var([1, 0.9, 0.8], 64),  pshift=-1, shape=1/2, sample=8, dur=1, delay=0.5, hpf=250, room=1/2, mix=1/3, amplify=P[0, 0, 1, 0], amp=1)
c4 >> loop(s + 'HAMMERS/15.wav', 1, rate=0.95, sus=0.25, dur=2, delay=[0, 1/2],
    lpf=expvar([7000, 3000], [64, 0]) * 0 + 400,
    amplify=2/3,
    amp=1
)
gC = Group(c1, c2, c3, c4)
gC.amp=0

gC.amp=1

gC.amp=linvar([1, 0, 0], [8, 0, inf], start=Clock.now())
d1 >> siren(oct=6, dur=16, chop=64, formant=[0, [0, 1], 0, [2, [0, 3]]], shape=2/3, room=3/4, mix=3/4, pan=PSine(32), amplify=4/5, amp=1)

gC.amp=1

d1.amp=0
e1 >> rlead(tremolo=P[2, 4], oct=3, hpf=140, dur=4, seqnote1=3, seqnote2=5, seqnote3=7, bps=3, formant=(0, 1), amplify=(0.5, 0.25), amp=1)
e2 >> hoover(oct=4,
    lpf=1800, lpr=expvar([4/5, 3/8], 32),
    coarse=4,
    echo=P[var([0, 1/2], 64), 0, [1, 0], 0], echotime=var([2, 4], [6, 2]),
    bend=[0, [0, 1/3], 0, [1/8, 0]],
    hpf=var.hpf,
    room=3/5, mix=var([1/3, 1/2],[6, 2]), pan=[-2/3, 2/3],
    amplify=2/7*linvar([[0.2, 1, 0.3], 1], 64), 
    amp=1
)
gE = Group(e1, e2)

gE.amp=0


f1 >> loop(s + 'BASS/2.wav', 0, sus=1.25, dur=1.5, hpf=300, lpf=expvar([10000, 1000], 24), amp=1.75 * 0.35)
f2 >> loop(s + 'HAMMERS/15.wav', 1, rate=var([1, 1.1], 128), sus=1, dur=1, hpf=var.hpf, lpf=expvar([7000, 3000], [64, 0]), amplify=0.3, amp=0.3)
f3 >> loop(s + 'HAMMERS/13.wav', 1, rate=1, sus=1, dur=1, hpf=var.hpf, amplify=f2.amplify*1/4, amp=1)
f4 >> play('n', sample=3, dur=0.25, delay=0.1, hpf=var.hpf, amplify=P[0.1, 0.2, 1, 0.5] * expvar([0, 1], 256), amp=1)
f5 >> play('~', sample=6, dur=0.25, pan=PWhite(-0.5, 0.5), amplify=linvar([0, 0, 1], [128, 128, 0]), amp=1)
f6 >> loop(s + 'PERCUSSION/HATS/1.wav', 0, sus=1, dur=1, amplify=var([0, 1], [32, 64, 64, 128]) * 1/5, amp=1)
f7 >> loop(s + 'PERCUSSION/HATS/2.wav', 0, sus=0.25, dur=0.5, amplify=0.25 * expvar([0, 1, 1], 128) * 0.8, amp=1)
gF = Group(f1, f2, f3, f4, f5, f6, f7)
gF.amp = linvar([0, 1, 1], [32, 0, inf], start=Clock.now())
gC.amp = linvar([1, 0, 0], [32, 0, inf], start=Clock.now())

gF.amp=0

gF.amp = linvar([1, 0, 0], [32, 0, inf], start=Clock.now())


g1 >> filthysaw(oct=5, dur=3, chop=6, bend=P[0, [0, -1/8, 0, -1/2]], drive=1/8, formant=2, lpf=linvar([400, 2400], 32), lpr=0.7, amplify=2/8*var([1, 0], [56, 8, 31, 1, 28, 4]), amp=1)
g2 >> play('n', sample=1, rate=0.95, lpf=1200, dur=0.25, echo=P[[0.125, 0], 0, 0, 0, 0], amp=1)
g3 >> play('H', sample=PRand([1, 7], 0.25), pan=PWhite(-0.5, 0.5), dur=0.5, room=1, mix=0.25, amplify=PWhite(0, 1) * var([1, 0], [1, 3, 2, 5, 1.5, 7, 2.5, 5, 1, 5]) * expvar([0, 0.5], 32) * 0.75, amp=1)
gG = Group(g1, g2, g3)

gG.amp = 0

gF.amp = linvar([1, 0, 0], [32, 0, inf], start=Clock.now())
gC.amp = var([0, 1], [32, inf], start=Clock.now())


h1 >> arpy(var([-4, -2], 32), oct=6, dur=var([3/2, PDur(9, [12, 4])], [12, 4]), sus=1/4, chop=8, formant=7, drive=1/6, room=2/3, mix=3/4, pan=PWhite(-2/3, 2/3), amplify=2/3 * 0.4, amp=1)

h1.amp=0

i1 >> loop(s + 'HAMMERS/22.wav', 1, dur=PDur(3, 8), sus=0.5, hpf=var.hpf, amplify=P[1, [expvar([0.5, 0], 64), expvar([0, 0.75], 32) * var([1, 0], 3)], expvar([1, 1, 0], 64)] * var([0, 1], [3, 1, 3, 1, 2, 2, 2, 2, 1, 3, 1, 3]) * 0.2, amp=1)
i2 >> loop(s + 'HAMMERS/23.wav', rate=var([1, 1.1], 128), sus=1, dur=1, hpf=var.hpf, lpf=expvar([2000, 3000, 4000], 64), amplify=0.75, amp=1)
i3 >> loop(s + 'HAMMERS/23.wav', 1, rate=1, sus=1, dur=0.25, hpf=var.hpf, lpf=2500, amplify=expvar([0, 1], [1, 0]) * 0.5, amp=1)
i4 >> loop(s + 'PERCUSSION/DRUM_FX/10.wav', 1, rate=1, sus=expvar([0.25, 1], 196), dur=1, delay=0.1, hpf=var.hpf, lpf=0, amplify=P[expvar([0, 1, 1], 128), 1] * 0.45 * expvar([0, [0, 1], 1], 256), amp=1)
gI = Group(i1, i2, i3, i4)
gI.amp=0

gI.amp=1
gC.amp=0

gI.amp = linvar([1, 0, 0], [32, 0, inf], start=Clock.now())

j1 >> play('*', sample=5, dur=1, amplify=P[0, 1] * expvar([0, [1, 0], 1], [64, 64, 128, 128]) * 0.6, amp=1)

j1.amp=1

k1 >> loop(s + 'PERCUSSION/PERC_RHYTHMS/23.wav', 1, rate=1, tremolo=2, dur=2, delay=1, sus=1, coarse=var([0, [0, 2]], [16, 64, 32]), hpf=var.hpf, pan=PWhite(-0.75, 0.75), lpf=expvar([2000, 5000], 16), amplify=0.75 * expvar([1, [0, 0, 1]], 128), amp=1)
k2 >> loop(s + 'PERCUSSION/HATS/9.wav', rate=1, dur=1/2, sus=1/8, pan=(-0.75, 0.75), lpf=expvar([1200, 2500], 16), room=2/3, mix=1/3, amplify=0.5 * expvar([1, 0], [64, 128, 256, 32, 32]), amp=1)
k3 >> loop(s + 'PERCUSSION/HATS/19.wav', rate=1, dur=1/2, sus=1/8, pan=(-0.75, 0.75), lpf=expvar([1200, 2500], 16), room=2/3, mix=1/3, amplify=2/3 * 0.55 * var([0, 1], [64, 32, 32, 128]), amp=1)
gK = Group(k1, k2, k3)
gK.amp=0

gK.amp = linvar([0, 1, 0], [16, 0, inf], start=Clock.now())
gI.amp = 0

gK.amp = linvar([1, 0, 0], [32, 0, inf], start=Clock.now())

l1 >> loop(s + 'HAMMERS/25.wav', 1, rate=var([1, 1.1], 64), sus=1, dur=1, hpf=var.hpf, lpf=0, amplify=3/4, amp=1)
l2 >> loop(s + 'HAMMERS/28.wav', 1, bend=var([0, 0.1], [60, 4, 31, 1, 32, 0, 31, 1, 64, 0]), rate=var([1, 1.1], 64), sus=1, dur=1, hpf=var.hpf, lpf=0, amp=1)
l4 >> loop(s + 'HAMMERS/36.wav', 1, rate=var([1, 1.1], 64), sus=1, dur=1, hpf=var.hpf, lpf=0, amp=1)
l5 >> loop(s + 'HAMMERS/36.wav', 1, rate=var([1, 1.1], 64), sus=0.4, dur=PDur(5, 8), hpf=var.hpf, lpf=3000, amplify=P[1, 1, [expvar([0, 0.5], 16), 1]] * 0.65, amp=1)
l6 >> loop(s + 'BASS/13.wav', 10, tremolo=4, sus=2, dur=2, hpf=0, lpf=expvar([10000, 1000], 24) * 0, amplify=0.3 * var([1, 0], 2) * expvar([0, 1], [64, 128, 64]), amp=1)
gL = Group(l1, l2, l3, l4, l5, l6)
gL.amp = 0
beats = Group(gF, gG, gK, gL, j1)
peeps = Group(d1, gE, h1)

beats.amp = 1

beats.hpf = linvar([0, 4000, 0], [8, 0, inf], start=Clock.now())
gL.amp = linvar([1, 0, 0], [32, 0, inf], start=Clock.now())

beats.amp = 0
gC.amp = 1
j1.amp = 1
h1.amp = 0
gF.amp = linvar([0, 1, 1], [16, 0, inf], start=Clock.now())
gK.amp = 1

gA.amp = 0
gB.amp = 0

gG.amp = 1
gC.amp = 0
h1.amp = 0

beats.amp = 1

peeps.amp = linvar([0, 1, 1], [16, 0, inf], start=Clock.now())
beats.hpf = linvar([0, 8000, 0], [16, 0, inf], start=Clock.now())

beats.hpf = linvar([0, 8000, 0], [8, 0, inf], start=Clock.now())
gA.amp = linvar([0, 1, 1], [8, 0, inf], start=Clock.now())
gB.amp = linvar([0, 1, 1], [8, 0, inf], start=Clock.now())
peeps.amp = var([1, 0], [8, inf], start=Clock.now())

beats.amp = linvar([1, 0, 0], [16, 0, inf], start=Clock.now())
peeps.amp = var([0, 1], [8, inf], start=Clock.now())

peeps.amp = linvar([1, 0, 0], [64, 0, inf], start=Clock.now())

gA.amp=linvar([1, 0, 0], [16, 0, inf], start=Clock.now())
gB.amp=linvar([1, 0, 0], [32, 0, inf], start=Clock.now())
