#IndustrialCrackDown
Clock.set_time(0)

Clock.bpm = 134
#SCALE
Scale.default = Scale.melodicMinor
#NOTES
var.bassnt = var([0,2,0,2,0,2,3,4,0,2,0,2,0,2,3,4])
var.chords = var([(var.bassnt[0],var.bassnt[0]+1),(var.bassnt[1],var.bassnt[1]+2),(var.bassnt[2],var.bassnt[2]+1),(var.bassnt[3],var.bassnt[3]+[0,2])])
#TIMING
var.int=var([1, 0],[256, 0, 0, 256, 0, 256, 0, 512, 0, 256, 0, 256, 0, 512, 0, 256])
var.brk1=var([1, 0],[0, 256, 256, 0, 0, 256, 0, 512, 0, 256, 0, 256, 0, 512, 0, 256])
var.bld1=var([1, 0],[0, 256, 0, 256, 256, 0, 0, 512, 0, 256, 0, 256, 0, 512, 0, 256])
var.drp1=var([1, 0],[0, 256, 0, 256, 0, 256, 512, 0, 0, 256, 0, 256, 0, 512, 0, 256])
var.brk2=var([1, 0],[0, 256, 0, 256, 0, 256, 0, 512, 256, 0, 0, 256, 0, 512, 0, 256])
var.bld2=var([1, 0],[0, 256, 0, 256, 0, 256, 0, 512, 0, 256, 256, 0, 0, 512, 0, 256])
var.drp2=var([1, 0],[0, 256, 0, 256, 0, 256, 0, 512, 0, 256, 0, 256, 512, 0, 0, 256])
var.out=var([1, 0],[0, 256, 0, 256, 0, 256, 0, 512, 0, 256, 0, 256, 0, 512, 256, 0])
var.brks = var([1, 0], [28, 4, 31, 1, 31, 1, 56, 8])
#SYNTHS
s1 >> pmcrotal(var.bassnt, oct=3, dur=8, sus=s1.dur*2, echo=1/2, chop=s1.dur*32, vib=expvar([0, 8],32), bend=1/12, lpf=PxRand([800, 1200],PRand([16, 32, 46, 128])), room=2/3, mix=1/3, pan=(-2/3,2/3), amplify=1, amp=1*(var.int+var.brk1+var.brk2+var.out))
s2 >> rsin(var.bassnt, oct=4, dur=[1, 4, 3, 8], sus=var([PRand([12, 8, 16], 64),s2.dur*2],16), coarse=1, slide=[0, 0, 0, PRand([0, 1/3, 1/2, 2/3, -1/2, -2/3, -1])], room=2/3, mix=1/2, pan=PWhite(-2/3, 2/3), amplify=var([0,4/12],[64, 192]), amp=1*(var.int+var.brk1+var.brk2+var.out))
s3 >> bchaos(PxRand([-3,3]), dur=PRand([4,6,8,12,16]), formant=3, bend=1/2, room=2/3, mix=1/2, pan=expvar([PWhite(-1, 1),PWhite(-1, 1)],[PxRand([4, 8, 16]), 0]), amplify=var([0,PRand([0, 2/7])],PRand([8, 16, 32, 64])), amp=1*(var.brk1+var.brk2+var.drp2))
s4 >> steeldrum(var.bassnt, oct=4, dur=16, sus=16, chop=2, echo=1/2, formant=PRand([1, 3, 4]), swell=1/2, room=3/4, mix=1/2, pan=PWhite(-2/3, 2/3), amplify=linvar([1/5, 1/4], 128), amp=1*(var.brk1+var.bld1))
s5 >> cluster(var.bassnt, oct=4, dur=16, sus=12, echo=1/2, shape=3/4, formant=0, swell=1, room=3/4, mix=1/2, pan=PWhite(-2/3, 2/3), amplify=expvar([3/5, 3/4], 128), amp=1*(var.brk2+var.bld2))
s6 >> ppad(var.chords, oct=7, dur=2, sus=4, shape=1/4, bend=[0,[0,1,3]], benddelay=[0,1/4], chop=PRand([2,4,8],4), tremolo=PRand([0,1,3,4],8), slide=expvar([5/7, 1/7], [PRand([2,4,8,16]),0]), lpf=linvar([0,1700],[4,4,4,PRand([4,8,16])]), room=4/5, mix=1/2, pan=PWhite(-2/3, 2/3), amplify=var([0,PRand([1/12, 1/8])],PRand([8, 16, 32])), amp=1*(var.brk1+var.bld1))
s7 >> filthysaw(var.bassnt, oct=var([3,4],128), dur=var([8,4],64), shape=expvar([1/8,2/9],[16,0]), room=2/3, mix=1/3, pan=(-2/3, 2/3), amplify=4/9, amp=1*(var.bld1+var.drp1+var.bld2+var.drp2))
s8 >> laserbeam([var.chords[0],var.chords[1],var.chords[2],var.chords[3]], oct=8, dur=16, atk=1/100, chop=64, vib=[0,0,0,4], lpf=P[[2500, 1750, 2250, 2000], 2000, [3500, 2500], [1850, 1750]], room=1, mix=1/2, pan=PWhite(-3/4,3/4), amplify=1/12, amp=1*(var.brk1+var.brk2))
s9 >> hoover(var.chords, oct=(5,6), dur=4, sus=4, chop=var([0,2,4,8],64), shape=1/3, room=2/3, mix=1/2, pan=(-2/3,2/3), amplify=3/18, amp=1*(var.bld1+var.drp1))
s0 >> triwave(var.chords, oct=6, dur=1, sus=2, chop=3, room=3/4, mix=1/2, amplify=3/7, amp=1*(var.brk1+var.brk2))
t1 >> phazer(var.bassnt, oct=var([3,7],[PRand([30, 6, 2, 14]), 2]), dur=1/4, sus=2/3, lpf=1600, formant=var([3,0]), coarse=PRand([2,3,4]), room=4/5, mix=1/2, amplify=5/7, amp=1*(var.brk2+var.bld2))
t2 >> wsaw(var.bassnt, oct=4, dur=4, sus=4, atk=1/20, coarse=16, amplify=5/8, amp=1*(var.bld2+var.drp2))
t3 >> moogpluck(P[3:0]+var([0,-1],32), oct=6, dur=1/4, shape=1/7, formant=expvar([0,5],[16,0]), pan=PWhite(-1,1), amplify=3/12, amp=1*(var.bld2+var.drp2))
#SAMPLES
b1 >> play("A", dur=1, rate=var([1, 4/5], [28, 4, 56, 8, 31, 1, 31, 1]), sample=var([3, 5], [28, 4, 56, 8, 31, 1, 31, 1]), amplify=var([3/5,0],[224, 32]), amp=1*(var.drp1+var.drp2))
b2 >> play("A", dur=1, sample=8, rate=4/5, amplify=4/5*var.brks, amp=1*(var.drp1+var.drp2))
b3 >> play("A", dur=2, delay=[0,1/2], rate=1, sample=var([20,18],[60,4]), amplify=4/5*var.brks, amp=1*(var.bld1+var.bld2))
b4 >> play("i", dur=1, delay=1/2, sample=6, amplify=4/5 * var.brks, amp=1*(var.drp1+var.drp2)).sometimes("stutter", 2, sample=3, delay=[0, 3/2]).every(64, "amen")
b5 >> play("i", dur=1, delay=[0, [0, 1/4]], rate=3/4, sample=var([5,6], [128, 256, 64, 512, 256, 128, 256, 512, 512, 256]), amplify=4/5 * var.brks, amp=var([0, 1], [64, [64, 128], [32, 64], [32, 64, 128], 64, 128, 64, [64, 32]])*(var.drp1+var.drp2))
b6 >> play("O", dur=2, delay=1, sample=27, room=3/4, mix=1/3, amplify=2/3, amp=1*(var.bld1+var.bld2)).rarely("stutter",2,delay=[0])
b7 >> play(":", dur=1/2, sample=8, rate=7/5, amplify=1/2, amp=1*(var.bld1+var.drp1+var.bld2+var.drp2)).sometimes("stutter", 2).every(32, "stutter", 3)
b8 >> play("a", dur=1/2, sample=6, rate=2, cut=var([1/4, 1/2, 3/4, 4/5, 1/2], [[32, 8, 4, 4], [16, 8], 32, [8, 16], 8, 16, 8, 4, 4]), amplify=var([0, 3/5], 128)*var.brks, amp=1*(var.drp1+var.bld2+var.drp2
))
b9 >> play("Y", sample=var([0, 2, 4, 0, 6],[32, 64, 16, 128, 64, 32, 64, 128, 128, 64]), lpf=expvar([400,PxRand([900,2400])],PRand([16,32,64,128])), room=3/4, mix=2/5, amplify=2/5*var.brks, amp=1*(var.drp1+var.drp2))
b0 >> play("Z", dur=1, rate=1, sample=var([13,16],4), pshift=1,amplify=1/2 * var([0, 1], [28, 4, 60, 4, 30, 2, 31, 1, 30, 2, 60, 2]), amp=1*(var.drp1+var.drp2))
c1 >> play("t", dur=2, sample=17, drive=1/12, echo=[0,0,0,1/2], room=2/3, mix=1/2, amplify=2/4, amp=1*(var.brk1+var.brk2))
c2 >> play("r", dur=var([1, 2, 4], PRand([4, 8, 16, 32])), delay=[0, 0, [1/4, 3/4], 0, [1/8, 1/2], 0, 1/2, 0], sample=3, room=4/5, mix=1/2, amplify=3/5, amp=1*(var.brk1+var.bld1))
c3 >> play("R", dur=var([1, 2], PRand([4, 8, 16, 32, 64])), sample=4, rate=[4/5, PRand([4/5, 1, 6/5], seed=3), 4/5, PRand([4/5, 1, 6/5], seed=4)], room=1, mix=1/2, amplify=3/5, amp=1*(var.brk1+var.bld1))
c4 >> play("T", dur=var([2, 4], PRand([4, 8, 16, 32])), delay=[0, 0, [1/4, 3/4], 0, [1/8, 1/2], 0, 1/2, 0], rate=2/3, sample=12, room=4/5, mix=1/2, amplify=3/5, amp=1*(var.brk2+var.bld2))
c5 >> play("T", dur=var([1, 2], PRand([4, 8, 16, 32, 64])), cut=1, rate=[4/5, PRand([4/5, 1, 6/5], seed=3), 4/5, PRand([4/5, 1, 6/5], seed=4)], sample=6, room=1, mix=1/2, amplify=3/5, amp=1*(var.brk2+var.bld2))
c6 >> play("N",dur=1/2,delay=1/2,sample=2,
    lpf=var([0,linvar([1200,4000],[8,0])],[64, 128, 32, 256, 128, 64, 128, 256, 256, 128]),
    drive=var([0,linvar([0,1/8],[8,0])],[64, 128, 32, 256, 128, 64, 128, 256, 256, 128]), 
    pshift=var([0,1],[[32, 8, 4, 4], [16, 8], 32, [8, 16], 8, 16, 8, 4, 4]),
    amplify=var([2/4,2/5],[64, 32, 128, 64, 128])*var.brks,
    amp=1*(var.drp1+var.drp2))
d1 >> play("A", dur=var([2,1,1/2,1/4,4],[8,8,8,4,4]), sample=3, amplify=var([0, 3/5],[224, 32]), amp=1*(var.bld1+var.brk2+var.bld2))
