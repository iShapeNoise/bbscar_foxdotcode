#The Zone

#NOTE
Root.default.set(var([0, -2],PRand([128, 256, 512]), seed=20))
#SYNTHS
s1 >> dafbass(dur=PRand([1, 4],PxRand([8, 32])), atk=1/8, sus=3/2, lpf=expvar([30, 2800], PRand([4, 8, 16, 32])), lpr=1/2, room=2/3, mix=1/2, amplify=linvar([5/8, 1/8],PRand([8, 16, 32, 64])), amp=var([1, 0], [PRand([16, 32, 64, 128], seed=5),[[64, 128], 32, 64, 32, 128]]))
s2 >> drone(oct=4, dur=32, lpf=[700, 500, 800, 300], lpr=1/3, room=2/3, mix=1/2, amplify=5/8, amp=var([1,0],PRand([16, 32, 64, 128], seed=8)))
s3 >> shore(oct=4, dur=16, atk=2, shape=1/8, bend=1/3, tremolo=PRand([3, 6]), lpf=PRand([200, 300, 400], seed=2), lpr=1/2, room=2/3, mix=1/2, amplify=linvar([1/4, 2],PRand([8, 16, 32, 64])), amp=var([0, 1],PRand([16, 32, 64, 128],seed=18)))
s4 >> bounce(oct=PRand([8, 9], seed=45), dur=PRand([1, 2],PRand([1, 2, 4, 8],seed=3)), formant=4, bend=1/9, benddelay=1/8, room=3/4, mix=expvar([1/2, 1], 32), pan=PWhite(-2/3, 2/3), amplify=var([0, 1/9], PRand([16, 32, 64], seed=9)), amp=var([1, 0], [PRand([4, 8, 16, 32, 64], seed=21), 64]))
s5 >> supersaw(oct=3, dur=16, lpf=800, lpr=1/2, slide=[0, 1/2, 0, 1], room=3/4, mix=1/2, amplify=2/12, amp=var([0, 1], PRand([8, 16, 32, 64], seed=13))).spread()
s6 >> click(oct=PRand([8, 9], seed=58), dur=16, chop=64, room=3/4, mix=expvar([1/2, 2/3], 8), amplify=linvar([0, 1/20], 16), amp=var([0, 1], PRand([8, 16, 32, 64],seed=13)))
s7 >> bbass(oct=4, dur=16, formant=1, slide=[0, 0, 0, 1/2], room=2/3, mix=1/2, amplify=1/8, amp=var([1, 0],PRand([16, 32, 64, 128], seed=22)))
s8 >> flute(2, oct=3, dur=PRand([8, 16, 32], seed=10), shape=expvar([1/8, 2/8], [PRand([8, 16, 32]), 0], seed=10), hpf=800, vib=4, bend=[0, PxRand(1, 5)/(1/8), 0, PxRand(1,5)*(1/8)], room=1, mix=2/3, pan=PWhite(-2/3, 2/3), amplify=2/8, amp=var([1, 0], [PRand([8, 16, 32, 64], seed=13),[32, 64, 64, [64, 128], [128, 64], 32]]))
s9 >> wobble(oct=3, dur=8, echo=[0, 1/2], echotime=4, room=1/4, mix=1/2, pan=(-2/3, 2/3), amplify=2/9, amp=var([0, 1], [[32, 16, 32, 64, 32, 32, 64, 32], PRand([8, 16, 32, 64],seed=13)]))
s0 >> cluster(oct=var(PRand([6, 7]), 16), dur=16, echo=1, echotime=12, chop=[0, 3, 6], room=3/4, mix=1/2, amplify=PRand([0, 1/9], 16), amp=var([1,0],[PRand([8, 16, 32, 64], seed=1),[32, 64, [128, 64], 32]]))
t1 >> blips(oct=3, dur=8, formant=1, echo=PxRand([0, [1/2, 1]], 32), echotime=4, fmod=1/4, room=3/4, mix=4/5, pan=PSine(16)+(-1/2, 1/2), amplify=2/8, amp=var([1, 0],PRand([8, 16, 32, 64, 128])))
t2 >> bphase(oct=PRand([4, 9], seed=6), dur=24, chop=32, slide=2, hpf=1200, hpr=1/4, lpf=1200, lpr=1/2, room=4/5, mix=5/6, pan=PWhite(-2/3, 2/3), amplify=1/12, amp=var([1, 0], [PRand([16, 32, 64, [32, 128]], seed=38),[16, 32, 64]]))
t3 >> triwave(oct=4, dur=1, chop=PRand([4, 8], 4), lpf=linvar([200, 1200], 4), lpr=1/3, room=4/5, mix=1/2, pan=(-4/5, 4/5), amplify=expvar([0, 2/8], 16), amp=var([1, 0],[PRand([16, 32, 64, [128, 64]], seed=42),[16, 32, 64]]))
