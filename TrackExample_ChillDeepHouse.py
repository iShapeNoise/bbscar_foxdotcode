#Chill Deep House Example

#Tempo
Clock.bpm=100
Clock.set_time(0)
#Root note
Root.default.set(-4)
#Default scale
Scale.default=Scale.minor
print(Scale.default)
#Notes
bassl=[0,-1,2,2,1,0,-1]
bassl2=[0,-1,-2,-2,-3,-2,-1]
chords=[(0,2,4), (-1,1,3), (-2,0,2), (-2,0,2), (-3,1,3),(-2,0,2), (-1,1,3)]
melody=[7,6,4,2,1,1,2,3,2,2,3,4,2,1,1,0,0,1,2]
mthree=[1,2,0]
#TIMING
var.int=var([1,0],[32,0,0,32,0,32,0,64,0,32,0,32,0,64,0,32])
var.brk=var([1,0],[0,32,32,0,0,32,0,64,32,0,0,32,0,64,0,32])
var.bld=var([1,0],[0,32,0,32,32,0,0,64,0,32,32,0,0,64,0,32])
var.drp=var([1,0],[0,32,0,32,0,32,64,0,0,32,0,32,64,0,0,32])
var.out=var([1,0],[0,32,0,32,0,32,0,64,0,32,0,32,0,64,32,0])
var.brks=var([1,0],[28,4,30,2,14,2,12,4])
#SYNTHS
p1 >> piano(bassl, oct=4, dur=[4,15/10,25/10,4,15/10,15/10,1], amplify=3/5*var([0,1],32), amp=1*(var.drp+var.out))
p2 >> piano(chords, oct=5, dur=[4,15/10,25/10,4,15/10,15/10,1], amplify=[2/5,1/5,3/5,2/6,1/7], amp=1*(var.drp+var.out))
p3 >> piano(PShuf(melody), oct=5,dur=PShuf([1,3/2,1/2,1/2,1,3/4,3/4,1/2,5/4,3/4,1/2,1,1,3/4,3/4,1,1/2,1/2,3/2]), amplify=[3/5,2/5,3/6,3/4,4/5,3/6], amp=1*var.drp+var.out)
s1 >> prophet(bassl, oct=5, dur=[4,15/10,25/10,4,15/10,15/10,1], shape=1/7, chop=([0,2,4,6],[8,4,2,2]), lpf=1500, amplify=4/5, amp=1*var.drp)
s2 >> prophet(chords, oct=6, dur=[4,15/10,25/10,4,15/10,15/10,1], chop=([0,2,4],[8,6,2]), lpf=linvar([1200,2000],16), pan=PWhite(-1,1), amplify=4/5, amp=1*var.drp)
s3 >> sinepad(bassl, oct=4, dur=[4,15/10,25/10,4,15/10,15/10,1], amplify=4/5, amp=1*(var.brk+var.bld))
s4 >> sinepad(chords,oct=5,dur=[4,15/10,25/10,4,15/10,15/10,1], formant=2, amplify=4/5, amp=1*(var.brk+var.bld))
s5 >> bass(bassl2, oct=5, dur=[4,15/10,25/10,4,15/10,15/10,1], drive=2/10, atk=0.01, lpf=900, amplify=2/8, amp=1*(var.brk+var.bld+var.out))
s6 >> dbass(Pvar(bassl2,[4,1.5,2.5,4,1.5,1.5,1]), oct=6, dur=PDur(3,8), sus=1, shape=1/8, cut=3/4, lpf=1300, amplify=2/7, amp=1*(var.bld+var.drp))
s7 >> bell(p3.degree, oct=6, dur=p3.dur,sus=1/2,room=2/3,mix=1/2,pan=PWhite(-1,1),amplify=1/6,amp=1*(var.bld+var.drp))
s8 >> marimba(p3.degree,oct=6,dur=[1,3/2,1/2,1/2,1,3/4,3/4,1/2,5/4,3/4,1/2,1,1,3/4,3/4,1,1/2,1/2,3/2],sus=3/2,echo=[0,1/2],shape=1/7,room=4/5,mix=1/2,amplify=2,amp=1*var.drp)
s9 >> pulse(Pvar([mthree,mthree+P[:2]],[24,8]), oct=[5,6], dur=PDur(3,8), sus=var([1/4,1/2],8),echo=[0,1/2], formant=linvar([0,1],32), chop=2, lpf=linvar([500,2000],[8,4,16,32,8,4,64,8]), room=2/3, mix=1/2, pan=[-2/3,2/3], amplify=4/7, amp=1*(var.int+var.brk+var.bld+var.out))
t1  >> crunch(bassl, oct=4, dur=P[1/2,1/4,1/4,3/4,1/4], sus=1/4, echo=[0,1/2], slide=-1, shape=2/3, hpf=1200, amplify=2/5, amp=1)
t2 >> crunch(melody, oct=5, dur=1/4, sus=1/4, shape=2/7, hpf=1000, amplify=PRand([0,2/5]), amp=1)
#SAMPLES
b1 >> play("V", dur=1, sample=0, lpf=900, lpr=4/5, amplify=2/3, amp=1*(var.bld+var.drp+var.out))
b2 >> play("V", dur=1, sample=0, hpf=400, lpr=4/5, amplify=2/3, amp=1*(var.int+var.brk+var.out))
b3 >> play("%", dur=1, sus=2/8, rate=1, lpf=600, amplify=3/5, amp=1*(var.int+var.brk))
b4 >> play("-", dur=1, sample=-1, delay=1/2, amplify=4/5, amp=1*(var.int+var.brk+var.bld+var.drp))
b5 >> play("[ss]", dur=1, delay=1/2, sample=2, lpf=2600, room=3/4, mix=1/2, pan=(-5/7,5/7), amplify=P[0,3/5,1/2,2/5,1/2,1/3,4/5]*1/5, amp=1*(var.bld+var.drp))
b6 >> play("*", dur=2, sample=0, rate=7/5, echo=var([0,6/5],[14,2]), room=2/3, mix=1/3, amplify=4/7, amp=1*(var.int+var.brk+var.bld+var.drp))
b7 >> play("n", dur=1, delay=1/2, sample=5, amplify=PRand([1/4,1/3,1/5]), amp=1*(var.int+var.brk)).every(32,"stutter",2)
#b8 >> play("C", dur=[7,1,4,3,3,2], sample=3, vib=4, lpf=900, lpr=4/5, room=4/5, mix=1/2, amplify=var([0,3/8],32), amp=1*var.drp)
#b9 >> play("C", dur=2, sample=0, pshift=3, lpf=PWhite(800,1600), lpr=5/6, room=1/3, mix=1/2, amplify=var([2/7,0],16), amp=1*var.bld)
#b0 >> play("C", dur=1/2, cut=1/2, sample=3, pshift=-4, vib=2, lpf=linvar([200,400],1), lpr=1/5, room=4/5, mix=1/2, amplify=var([0,3/8],16), amp=1*var.bld)

print(SynthDefs)

print(Attributes)
