#TrackExample_SynthWave

Clock.set_time(0)

#TEMPO
Clock.bpm = 85
#ROOT
Root.default.set(4)
#SCALES
Scale.default = Scale.phrygian
print(Scale.default)
print(SynthDefs)
#NOTES
bl = P[2, 1, 0, 1, 2, 3, 2, 1, 0, -1, -1, 3]
bl2 = P[2, 3]
chrds = P[(2, 4, 7), (1, 3, 5), (2, 4, 6), (3, 5, 7), (-1, 2, 4), (2, 4, 7), (0, 2, 4), (-1, 1, 3)]
chrds2 = P[(3, 4, 7, 9), (3, 4, 7, 10)]
mld1 = P[7]
mld2 = P[6, 7, 3, 4, 5, 2, 0, 2, 0, -2, -2, 2, 3, 2, 0, 3, 4]
mld3 = P[2, 0, -2, -1, 0, 3, 2]
dbl = P[3/4, 3+1/4, 3/4, 1+1/4, 3/4, 1+1/4, 3/4, 3+1/4, 3/4, 1+1/4, 3/4, 1+1/4]
dbl2 = P[3/4, 3+1/4]
dchrd = P[3/4, 3+1/4]
dmld1 = P[3/4, 3+1/4, 3/4, 2, 1+1/4, 3/4, 3+1/4, 3/4, 2, 1+1/4]
dmld2 = P[3/4, 3/4, 2+1/2, 3/4, 1+1/4, 3/4, 1+1/4, 3/4, 2+1/4, 1/2, 1/2, 3/4, 1+1/4, 3/4, 3/4, 1/4, 1/4]
dmld3 = P[1, 1, 1, 1, 1, 1, 2]
#TIMING
#Intro 64, Main 128, Outro 48 = 240
#SYNTHS
#Bass
s1 >> wsaw(Pvar([bl2, bl, bl2], [64, 128, 48]), oct=(3, 4), dur=Pvar([dbl2, dbl, dbl2], [64, 128, 48]), lpf=2800, amplify=2/9, amp=var([0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0], [32, 4, 4, 4, 4, 4, 12, 30, 2, 62, 2, 36, 4, 4, 4, 4, 28]))
s2 >> dafbass(Pvar([bl2, bl, bl2], [64, 128, 48]), oct=5, dur=Pvar([dbl2, dbl, dbl2], [64, 128, 48]), lpf=2000, amplify=2/9, amp=var([0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0], [32, 4, 4, 4, 4, 4, 12, 30, 2, 62, 2, 36, 4, 4, 4, 4, 28]))
#String
s3 >> varsaw(mld1, oct=(5, 6), dur=8, echo=2, echotime=2, lpf=900, pan=(-2/3, 2/3), room=2/3, mix=1/3, amplify=4/8, amp=var([1/3, linvar([1/3, 1], [32, 0 ]), 1, 1, 0, 1, 0, 1, 0, 1, 0], [32, 32, 32, 2, 32, 16, 14, 2, 64, 16])).spread()
#Melody
s4 >> prophet(mld2, oct=(4, 5), dur=dmld2, sus=dmld2-1/8, pan=(-2/3, 2/3), amplify=3/9, amp=var([0, 1, 0, 1, 0], [94, 34, 30, 34, 48]))  
#Piano
s5 >> piano(Pvar([bl2, bl, bl2], [64, 128, 48]), oct=4, dur=Pvar([dbl2, dbl, dbl2], [64, 128, 48]), lpf=0, amplify=4/9, amp=var([0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0], [32, 4, 4, 4, 4, 4, 4, 4, 4, 30, 2, 62, 2, 36, 4, 4, 4, 4, 4, 4, 20]))
s6 >> piano(Pvar([chrds2, chrds, chrds2], [64, 128, 48]), oct=5, dur=dchrd, amplify=4/7, amp=var([0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0], [32, 4, 4, 4, 4, 4, 4, 4, 4, 30, 2, 62, 2, 36, 4, 4, 4, 4, 4, 4, 20]))
s7 >> piano(mld2, oct=6, dur=dmld2, amplify=6/9, amp=var([0, 1, 0], [64, 96, 80]))
s8 >> epiano(Pvar([bl2, bl, bl2], [64, 128, 48]), oct=4, dur=Pvar([dbl2, dbl, dbl2], [64, 128, 48]), atk=1/2, sus=2, formant=1, fmod=1, amplify=4/9, amp=var([0, 1, 0], [64, 128, 48]))
#Bells
s9 >> zap(mld1, oct=6, dur=1/4, sus=1, atk=1/16, chop=9, lpf=linvar([400, 1400], 2), amplify=linvar([3/7, 2/7], 4), amp=var([0, 1, 0, 1, 0, 1, 0, 1, linvar([1, 0], [8, 0]), 0], [48, 16, 48, 16, 48, 16, 16, 16, 8, 8]))
s0 >> chimebell(mld1, oct=7, dur=1/2, sus=1/4, atk=1/4, chop=2, lpf=linvar([400, 900], 1), bend=[0, 1/12], room=3/4, mix=1/2, amplify=linvar([2/7, 3/7], 2), amp=var([0, 1, 0, 1, 0, 1, 0, 1, linvar([1, 0], [8, 0]), 0], [48, 16, 48, 16, 48, 16, 16, 16, 8, 8]))
#Organ
t1 >> tworgan4(mld1, oct=(6, 7), dur=dbl, room=3/4, mix=1/2, amplify=2/8, amp=var([0, 1, 0, 1, 0], [144, 14, 2, 32, 48]))
t2 >> tremsynth(mld3, oct=7, dur=dmld3, sus=2, fmod=2, lpf=2800, room=2/3, mix=1/2, amplify=2/12, amp=var([0, 1, 0, 1, 0, 1, 0], [112, 12, 20, 12, 20, 12, 52]))
#SAMPLES
b1 >> play("V", dur=1, sample=9, amplify=2/5, amp=var([1, 0, 1, 0, 1, 0], [56, 39, 57, 7, 57, 24]))
b2 >> play("X", dur=1, sample=1, amplify=2/5, amp=var([1, 0, 1, 0, 1, 0], [56, 39, 57, 7, 57, 24]))
b3 >> play("x", dur=2, delay=1, sample=15, amplify=1/3, amp=var([1, 0, 1, 0, 1, 0], [56, 39, 57, 7, 57, 24]))
b4 >> play("o", dur=2, sample=4, cut=1/4, verb=1/4, room=1/3, mix=1/3, amplify=3/5, amp=var([0, 1, 0, 1, 0], [32, 32, 32, 128, 16]))
b5 >> play("O", dur=2, sample=3, amplify=3/9, amp=var([0, 1, 0, 1, 0], [32, 32, 32, 128, 16]))
b6 >> play("*", dur=2, sample=11, amplify=3/4, amp=var([1, 0, 1, 0, 1, 0, 1, 0], [64, 16, 14, 2, 62, 2, 64, 16]))
b7 >> play("-", dur=1/4, sample=6, amplify=3/5, amp=var([1, 0, 1, 0, 1, 0, 1, 0], [64, 16, 14, 2, 62, 2, 64, 16]))
b8 >> play("=", dur=1, delay=[1/2, 1/4, 3/4, 1/2, (1/4, 1/2), 1/4, 3/4, 1/2], sample=5, rate=3/4, amplify=3/5, amp=var([1, 0, 1, 0, 1, 0, 1, 0], [64, 16, 14, 2, 62, 2, 64, 16]))
b9 >> play("=", dur=4, delay=3+1/2, sample=18, rate=1, amplify=var([3/8, 0], 4), amp=var([1, 0, 1, 0, 1, 0, 1, 0], [64, 16, 14, 2, 62, 2, 64, 16]))
b0 >> play("t", dur=1, delay=[(1/4, 3/4),(0, 1/2)], room=2/3, mix=1/2, amplify=var([0, 2/3, 0], [6, 2, 8]), amp=var([1, 0, 1, 0, 1, 0, 1, 0], [64, 16, 14, 2, 62, 2, 64, 16]))
c1 >> play("#", dur=1, cut=1/2, delay=[(0, 1/2), (0, 1/4, 1/2), (0, 1/3, 2/3), (0, 1/2)], rate=7/5, sample=25, lpf=expvar([400, 1200], 16), room=3/4, mix=1/2, amplify=1/12, amp=var([0, 1, 0, 1, 0, 1, 0], [32, 62, 2, 62, 2, 64, 16]))
c2 >> play("e", dur=1, delay=[1/2, 1/2, (0, 1/2), [1/2, (1/4, 1/2)]], sample=4, lpf=var([400]), amplify=2/8, amp=var([0, 1, 0, 1, 0, 1, 0], [32, 62, 2, 62, 2, 64, 16]))
c3 >> play("E", dur=16, sample=17, rate=3/4, formant=5, bend=1/16, hpf=1800, lpf=2800, room=3/4, mix=1/2, amplify=6/8, amp=1)
c4 >> play("E", dur=4, sample=17, rate=-3/4, formant=5, bend=1/16, hpf=1800, lpf=2400, room=3/4, mix=1/2, amplify=5/8, amp=var([0, 1, 0, 1, 0, 1, 0, 1, 0], [60, 4, 60, 4, 28, 4, 60, 4, 16]))
#sfx
c5 >> play("Y", dur=1, sample=0, echo=(1/2, [1/3, 2/3], 3/4), echoTime=4, formant=3, room=2/3, mix=expvar([1/3, 3/4], 8), pan=PWhite(-2/3, 2/3), amplify=var([linvar([4/8, 0], [8, 0]), 0], 8), amp=var([0, 1, 0, 1], [64, 128, 32, 16]))
c6 >> play("Y", dur=1, sample=0, echo=(1/2, [1/3, 2/3], 3/4), echoTime=4, rate=-1, formant=3, room=2/3, mix=expvar([1/3, 3/4], 8), pan=PWhite(-2/3, 2/3), amplify=linvar([4/8, 0], [8, 0]), amp=var([0, 1, 0, 1, 0, 1, 0], [24, 8, 24, 8, 152, 8, 16]))
#Vinylcracks
d1 >> play("z", dur=8, sample=34, rate=1, hpf=580, lpf=2800, amplify=4/9, amp=1)

