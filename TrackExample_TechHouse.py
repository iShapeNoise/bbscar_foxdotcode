#Tech House Example

#Tempo
Clock.bpm=125
Clock.set_time(0)
#Root note
Root.default.set(0)
#Default scale
Scale.default=Scale.chromatic
scale=Scale.default
print(Scale.default)
#Notes
bassl=[8,10,11,5,5]
#TIMING
var.int=var([1,0],[32,224])
var.brk=var([1,0],[0,32,32,0,0,128,32,0,0,32])
var.bld=var([1,0],[0,64,32,0,0,64,32,0,0,64])
var.drp=var([1,0],[0,96,64,0,0,96])
var.out=var([0,1],[224,32])
var.brks=var([1,0],[60,4,124,4,60,4])
#SYNTHS
s1 >> jbass(bassl,oct=4,dur=[1,1,1,1/2,1/2],sus=3/4,delay=1/2,shape=0,lpf=100,formant=(0,1),amplify=3/5*var.brks,amp=1)
s2 >> dbass(s1.degree,oct=4,dur=s1.dur,sus=2,coarse=1,delay=1/2,lpf=90,amplify=4/5*var.brks,amp=1)
s3 >> space(8,oct=(3,4),dur=16,sus=16,lpf=400,room=3/4,mix=1/2,pan=(-2/3,2/3),amplify=6/5*var.brks,amp=1)
s4 >> quin(8,oct=5,dur=16,sus=16,lpf=200,room=3/4,mix=1/2,pan=PWhite(-1,1),amplify=1*var.brks,amp=1)
#s5 >> 
#s6 >>
#s7 >>
#s8 >>
#s9 >>
#s0 >>
s1.amp=1*(var.drp)
s2.amp=1*(var.drp)
s3.amp=1*(var.bld+var.drp)
s4.amp=1*(var.bld+var.drp)
#s5.amp=1*
#s6.amp=1*
#s7.amp=1*
#s8.amp=1*
#s9.amp=1*
#s0.amp=1*
#SAMPLES
b1 >> play("X",dur=1,sample=0,rate=3/4,amplify=1*var.brks,amp=1)
b2 >> play("v",dur=1,sample=5,rate=1,lpf=1800,amplify=2/3*var.brks,amp=1)
b3 >> play("V",dur=1,sample=4,rate=3/5,amplify=2/3*var.brks,amp=1)
b4 >> play("o",dur=2,rate=6/5,sample=4,amplify=1*var.brks,amp=1)
b5 >> play("*",dur=2,sample=3,amplify=4/5*var.brks,amp=1)
b6 >> play("...([.o].)(.[.o])(.[.o])(.[.o])[.t]",dur=1/2,rate=1,sample=b6.degree.map({"o":5,"t":7}),amplify=2/5*var.brks,amp=1)
b7 >> play("-",dur=1,delay=1/2,rate=1,sample=3,amplify=6/5*var.brks,amp=1).every(16,"stutter",2)
b8 >> play("~",dur=2,delay=[1/4,1/2],sample=0,rate=7/5,amplify=1*var.brks,amp=1)
b9 >> play("S[ss]",dur=1/2,sample=b9.degree.map({"S":0,"s":1}),rate=7/5,amplify=b9.degree.map({"S":var([4/5,PRand([2/5,1/3])],[1/2,1+1/2]),"s":var([4/5,PRand([1/2,3/4])],[1/4,3/4])}),amp=1)
b0 >> play("</><~>",dur=1,delay=b0.degree.map({"/":0,"~":1/2}),sample=b0.degree.map({"/":0,"~":5}),amplify=b0.degree.map({"/":var([1,0],[1,15]),"~":var([0,3/5],[28,4])}),amp=1)
c1 >> play("r",dur=[3]+[[1/4,1/4,1/2],[1/4,1/2,1/4]]*2,delay=1/2,rate=7/4,sample=1,lpf=3200,vib=3,vibdepth=1/8,room=1/3,mix=1/3,pan=PWhite(-1,1),amplify=3/2*var.brks,amp=1)
c2 >> play("b",dur=4,delay=[3/4],sus=3/4,sample=1,coarse=3,formant=1,lpf=1600,amplify=3/8*var.brks,amp=1)
c3 >> play("+",dur=16,delay=1/2,sample=1,echo=(1,2,4),pshift=-24,swell=1/2,room=1,mix=1/2,pan=(-2/3,2/3),amplify=3/5*var.brks,amp=1)
c4 >> play("?",dur=16,sus=var([8,8,2,2]),cut=1,sample=1,pshift=3,shape=1/8,lpf=1200,amplify=6/5*var.brks,amp=1)
c5 >> play("Z",dur=[1/2,1/2,16],sus=[1/2,1/2,4],pshift=-3,coarse=2,lpf=500,formant=expvar([2,0],[16,0]),room=2/3,mix=1/2,pan=PWhite(-1,1),amplify=2/5*var.brks,amp=1)
gB1 = Group(b1,b2,b3)
gB1.rarely("stutter",2)
b1.amp=1*(var.brk+var.bld+var.drp)
b2.amp=1*(var.brk+var.bld+var.drp)
b3.amp=1*(var.brk+var.bld+var.drp)
b4.amp=1*(var.int+var.brk+var.bld+var.drp+var.out)
b5.amp=1*(var.int+var.brk+var.bld+var.drp+var.out)
b6.amp=1*(var.int+var.brk+var.bld+var.drp+var.out)
b7.amp=1*(var.int+var.brk+var.bld+var.drp+var.out)
b8.amp=1*(var.int+var.brk+var.bld+var.drp+var.out)
b9.amp=1*(var.bld+var.drp+var.out)
b0.amp=1*(var.bld+var.drp+var.out)
c1.amp=1*(var.brk+var.bld+var.drp+var.out)
c2.amp=1*(var.brk+var.bld+var.drp+var.out)
c3.amp=1*(var.bld+var.drp)
c4.amp=1*(var.brk+var.bld+var.drp+var.out)
c5.amp=1*(var.bld+var.drp)

print(SynthDefs)

print(Attributes)
