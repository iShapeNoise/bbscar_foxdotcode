#FoxDot_180422_choppyWater_session01
Clock.bpm=102
#NOTEZ
Root.default.set(3)
Scale.default.set=(Pvar([Scale.major,Scale.majorPentatonic],64))
scale = Scale.default
note = PRand(scale,seed=5)
note2 = PRand(scale,seed=8)
#TIMING
var.int=var([1,0],[32,0,0,32,0,32,0,64,0,32,0,32,0,64,0,32])
var.brk=var([1,0],[0,32,32,0,0,32,0,64,32,0,0,32,0,64,0,32])
var.bld=var([1,0],[0,32,0,32,32,0,0,64,0,32,32,0,0,64,0,32])
var.drp=var([1,0],[0,32,0,32,0,32,64,0,0,32,0,32,64,0,0,32])
var.out=var([1,0],[0,32,0,32,0,32,0,64,0,32,0,32,0,64,32,0])
var.brks=var([1,0],[28,4,30,2,14,2,24,8,12,4])
#SYNTHS
s1 >> bass(note,oct=(4,5),dur=PSum(3,2),sus=1/2,chop=2,verb=2/3,room=4/5,mix=linvar([1/3,1/2],8),pan=[-2/3,2/3],amplify=4/5,amp=1*(var.brk+var.bld+var.drp))
s2 >> spark(note,oct=(4,5),dur=var([1,PDur(3,5)],[24,8]),coarse=2,formant=2,verb=4/5,room=2/3,mix=1/2,amplify=4/5,amp=1*(var.bld+var.drp)).offbeat()
s3 >> piano(note2,oct=(5,6),dur=PRand([PSum(3,2),0.5,1],1),sus=1,pan=[-2/3,2/3],amplify=4/5,amp=1*(var.bld+var.drp))
s4 >> pluck(note,oct=(6,7),dur=PRand([PSum(3,2),PDur(3,5),1],1/2),sus=2/3,formant=2,pan=(-2/3,2/3),amplify=5/6,amp=1*(var.brk+var.bld))
s5 >> bell(note2,oct=(5,6),dur=PSum(5,4),amplify=2/7,amp=1*(var.brk+var.out))
s6 >> sitar(note,oct=(4,6),dur=PRand([2/4,1/4,PDur(3,5)]),pan=[-5,6],amplify=linvar([2/3,4/6],[6,2]),amp=1*var.drp)
s7 >> donk(note2,oct=4,dur=4,echo=1/2,formant=linvar([0,5],[16,0]),pan=PWhite(-1,1),amplify=2,amp=1*(var.int+var.bld+var.drp))
#PERCS N DRUMZ
b1 >> play("<(V*)(-[-V]-)><x>",sample=-1,chop=8,verb=2/3,room=5/7,mix=1/3,amplify=4/5*var.brks,amp=1*(var.bld+var.drp))
b2 >> play("..o.",rate=1,sample=3,room=var([1/7,1],[6,2]),mix=1/2,amplify=6/5*var.brks,amp=1*(var.brk+var.bld+var.drp))
b3 >> play("[ss].([ss].s.)",rate=1,sample=1,amplify=4/5,amp=1)
b4 >> play(".(..i)i(..[ii])",sample=4,amplify=4/5*var.brks,amp=1*(var.bld+var.drp))
b5 >> play("m.m.m(.[.m])",sample=var([1,2,3],[12,4]),rate=(4/5,1),pan=[-2/3,2/3],amplify=3/5*var.brks,amp=1)
b6 >> play("pp[pp][pp]",rate=PRange(2,10),lpf=linvar([300,4000],[8,0]),slide=var([0,[0,-1,0,1]],16),pan=[-3/4,3/4],amplify=var([2/3,1],[30,2])*var.brks,amp=1*(var.bld+var.drp))
b7 >> play("-[--]",sample=2,amplify=6/7*var.brks,amp=1*(var.brk+var.bld))
b8 >> play("~",dur=1,delay=1/2,sample=1,amplify=7/5*var.brks,amp=1*(var.bld+var.drp)).sometimes("stutter",2).every(16,"stutter",2)
b9 >> play("-",sample=2,rate=1,amplify=4/5,amp=1*(var.brk+var.bld+var.drp)).sometimes("stutter",2)
