# Bass House Track Example
Clock.set_time(0)
#TEMPO
Clock.bpm=130
#ROOT NOTE
Root.default.set(0)
#SCALE
Scale.default=Scale.chromatic
print(Scale.default)
#Create your own collection of notes as scale
scale=[0,1,3,5,7,8,9,10]
#NOTES
bassla=P[scale[0],scale[0],scale[1],scale[3]]
basslb=P[scale[3],scale[3],scale[1],scale[0]]
basslc=bassla-3
bassld=basslb-3
#SYNTHS
s1 >> lazer(Pvar([bassla,basslb],8), oct=var([(2,3),(3,4)],16), dur=1, sus=3/4, shape=2/5, formant=2, pan=(-2/3,2/3), amplify=var([0,1,0,1],4), amp=1)
s2 >> lazer(Pvar([bassla,basslb],8), oct=var([4,5],16), dur=1, sus=3/4, amplify=var([0,6/5,0,6/5],4), amp=1)
s3 >> scratch(oct=4, dur=1/2, shape=2/5, drive=2/8, formant=4, chop=2, pan=(-2/3,2/3), amplify=var([7/5,0],[2,6,2,6]), amp=1)
s4 >> dub(Pvar([bassla,basslb],8), oct=(5,6), dur=1, sus=3/4, pan=(-2/3,2/3), amplify=var([0,3/9,0,3/9],4), amp=1)
s5 >> dab(oct=4, dur=1/2, sus=2, formant=4, coarse=3, amplify=var([2/6,0],[3,5,3,5]), amp=1)
s6 >> twang(2, oct=6, dur=1, sus=1, formant=4, drive=1/8, lpf=800, amplify=var([0,1/8,0],[2,1,5]), amp=1)
s7 >> donk(P[:16], oct=(3,6), dur=1/4, drive=1/12, lpf=600, amplify=4/5, amp=1).reverse()
#s8 >> 
#s9 >> 
#Big Break
var.brk=var([0,1],[0,60,4,160,8,120])
#SAMPLES 
b1 >> play("X", dur=1, rate=1, sample=0, pan=(-2/3,2/3), amplify=var([6/5,0],[62,2])*var.brk, amp=1)
b2 >> play("V", dur=1, sample=5, rate=3/5, pshift=7, tremolo=1/4, drive=1/22, shape=8/5, amplify=var([4/5,0],[1,7]), amp=1)
b3 >> play("m", dur=1, sample=0, drive=1/20, shape=1, slide=var([0,3/4],8), pshift=-3, pan=(-1,1), amplify=var([3/7,0],[1,7]), amp=1)
#CLAP
b4 >> play("*", dur=2, sample=4, amplify=3/5*var.brk, amp=1)
b5 >> play("*", dur=2, sample=6, amplify=3/5*var.brk, amp=1)
b6 >> play("o", dur=2, delay=1/2, sample=7, rate=6/5, amplify=4/5*var.brk, amp=1)
#HIHAT
b7 >> play("-", dur=1, delay=1/2, rate=1, sample=2, amplify=6/5*var.brk, amp=1)
b8 >> play("=", dur=1, delay=1/2, sample=4, rate=7/5, amplify=4/5*var.brk, amp=1)
#SOUNDFX
b9 >> play("Z", dur=1, sample=1, formant=4, room=4/5, mix=1/2, pan=(-2/3,2/3), amplify=var([0,3/10,0],[3,1,20]), amp=1)
#SHAKER
b0 >> play("s", dur=1/4, rate=2, sample=[[2,4],0,1,3], shape=2/7, formant=4, amplify=5/8*var.brk, amp=1)
#VOICE
c1 >> play("?", dur=2, cut=3/5, sample=0, rate=1/2, pshift=6, amplify=var([0,2,0],[19,2,3])*var.brk, amp=1)
#CRASH
c2 >> play("~", dur=1, cut=4/5, delay=1/2, sample=2, pshift=2, amplify=5/7*var.brk, amp=1)
#SHAKERRHYTHM
c3 >> play("s", dur=1/2, rate=[1,3/4], sample=[3,4], coarse=[0,2], amplify=[4/5,2/5]*var.brk, amp=1)
#SNAREROLL
c4 >> play("i", dur=var([1,1/2,1/4,4],4), rate=1, hpf=linvar([2000,0],[16,0]), pshift=-2, amplify=7/5*var.brk, amp=1)
#OFFBEAT SNARE
c5 >> play("o", dur=1/2, delay=[1/4,1/4], amplify=var([0,0,1,0],[1,1,1,1]), amp=1)
#BREAKBEAT
d1 >> play("X", dur=2, delay=[0,1/2], rate=2, hpf=300, sample=0, amplify=var([0,6/5],[28,4]), amp=1)
d2 >> play("o", dur=2, delay=[1/2,0], amplify=var([0,3/4],[28,4]), amp=1)
d3 >> play("o", dur=1, delay=[1/4,1/2,0,[1/2]], sample=3, shape=1/8, amplify=var([0,2/3],[28,4]), amp=1)
d4 >> play("-", dur=1, delay=(0,[1/4,1/2,0,[1/2]]), amplify=var([0,4/5],[28,4]), amp=1)
d5 >> play("#", dur=1/2, sample=2, amplify=var([0,3/5],[28,4]), amp=1)
#SYTNHS TIMING
s1.amp=var([1,0],[0,64,64,0,0,96,64,0,0,64])
s2.amp=var([1,0],[0,64,64,0,0,96,64,0,0,64])
s3.amp=var([1,0],[0,64,64,0,0,96,64,0,0,64])
s4.amp=var([1,0],[0,64,64,0,0,96,64,0,0,64])
s5.amp=var([1,0],[0,64,64,0,0,96,64,0,0,64])
s6.amp=var([1,0],[0,64,64,0,0,96,64,0,0,64])
s7.amp=var([1,0],[0,92,4,28,4,124,4,28,4,64])
#s8.amp=var([1,0],[])
#s9.amp=var([1,0],[])
#SAMPLES TIMING 
b1.amp=var([1,0],[32,0,0,32,64,0,64,0,0,32,64,0,64,0]) 
b2.amp=var([1,0],[1,15,1,15,64,0,64,0,1,15,1,15,64,0,32,32])
b3.amp=var([1,0],[64,0,64,0,0,32,64,0,32,0,0,32])
gC = Group(b4,b5,b6)
gC.dur=var([2,1,2,1,2],[32,32,128,32,128])
b9.amp=var([1,0],[0,64,64,0,0,96,64,0,0,64])
b0.amp=var([1,0],[0,32,32,64,32,32,32,64,32])
#
c1.amp=var([1,0],[0,128,64,64,64,32])
c2.amp=var([1,0],[0,48,16,0,0,32,32,0,0,80,16,0,32,32,64])
c3.amp=var([1,0],[0,16,48,0,32,64,0,0,16,48,0,0,32,80,16])
c4.amp=var([1,0],[0,48,16,144,16,128])
c5.amp=var([1,0],[0,12,4,12,4,68,4,4,4,4,4,8,4,4,4,4,4,100,4,4,4,4,4,8,4,4,4,4,4,40])
#Little Breakbeat
d1.amp=var([1,0],[32,0,0,32,128,0,32,128,0])
d2.amp=var([1,0],[32,0,0,32,128,0,32,128,0])
d3.amp=var([1,0],[32,0,0,32,128,0,32,128,0])
d4.amp=var([1,0],[32,0,0,32,128,0,32,128,0])
d5.amp=var([1,0],[32,0,0,32,128,0,32,128,0])

print(128+32-16)

print(SynthDefs)

print(Attributes)
