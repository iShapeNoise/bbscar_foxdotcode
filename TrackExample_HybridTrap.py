#TrackExample_HybridTrap
Clock.set_time(0)

#TEMPO
Clock.bpm = 145
#SCALE
Scale.default = Scale.chromatic
#SYNTHS
#MPart1
s1 >> dub(Pvar([[6, 7], [5, 5, 6]], 16), oct=(6, 7), dur=Pvar([1, [1/2, 1/2, 1]], 16), atk=1/2, coarse=32, shape=1/8, hpf=900, hpr=1/2, lpf=1280, lpr=1/3, pan=(-2/3, 2/3), amplify=var([1, 0], [2, 14])*5/12, amp=var([0, 1, 0, 1, 0, 1, 0, 1, 0, 0], [48, 48, 16, 16, 80, 48, 16, 16, 32, 32]))
s2 >> dbass(Pvar([[6, 7], [5, 5, 6]], 16), oct=4, dur=Pvar([1, [1/2, 1/2, 1]], 16), sus=3/2, formant=(3, 0, 0, 3), pan=(-2/3, 2/3), amplify=var([1, 0], [2, 14])*3/7, amp=var([0, 1, 0, 1, 0, 1, 0, 1, 0, 0], [48, 48, 16, 16, 80, 48, 16, 16, 32, 32]))
#MPart2.1
s3 >> spacesaw(var([6, 1], 16), oct=var([3, 2], [5, 11]), dur=Pvar([1, 1/2, 1], [19, 1, 12]), sus=Pvar([7/8, 1/3, 7/8], [19, 1, 12]), shape=2/3, drive=1/8, hpf=500, pan=(-2/3, 2/3), amplify=var([0, 1, 0], [3, 11, 2])*3/6, amp=var([0, 1, 0, 1, 0, 0], [56, 2, 6, 64, 192, 32]))
s4 >> wsaw(var([6, 1], 16), oct=4, dur=Pvar([1, 1/2, 1], [19, 1, 12]), sus=Pvar([7/8, 1/3, 7/8], [19, 1, 12]), formant=4, drive=1/6, hpf=1000, pan=PWhite(-1, 1), amplify=var([0, 1, 0], [3, 11, 2])*2/9, amp=var([0, 1, 0, 1, 0, 0], [56, 2, 6, 64, 192, 32]))
#MPart2.2
s5 >> filthysaw(var([6, 1], 16), oct=var([5, 4], [5, 11]), dur=Pvar([1, 1/2, 1], [19, 1, 12]), shape=7/5, chop=var([0, 2, 0], [3, 11, 2]), lpf=linvar([400, 2400], 8), lpr=3/4, glide=2, amplify=var([0, 1, 0], [3, 11, 2])*5/6, amp=var([0, 1, 0, 1, 0, 0], [216, 2, 6, 64, 32, 32]))
s6 >> rsin(var([6, 1], 16), oct=(6, 8), dur=Pvar([1, 1/2, 1], [19, 1, 12]), shape=5/9, coarse=3, lpf=linvar([400, 3800], [8,0]), room=3/4, mix=1/2, pan=(-2/3, 2/3), amplify=var([0, 1, 0], [3, 11, 2])*2/10, amp=var([0, 1, 0, 1, 0, 0], [216, 2, 6, 64, 32, 32]))
#MPart3
s7 >> spick(Pvar([[7, 5], [7, 5]], 16), oct=Pvar([(5, 7), (7, 9)], 16), dur=1, sus=3/4, chop=16, hpf=1200, hpr=2/3, drive=5/9, amplify=var([0, 1], [14, 2])*5/7, amp=var([0, 1, 0, 1, 0, 0], [64, 64, 96, 64, 32, 32]))
#Bridge Melody
s8 >> noisynth(var([3, 3, 8, 3, 5]), oct=var([3, (3, 4), (3, 4), 3, (3, 4)], [32, 128, 32, 32, 96]), dur=PDur(3, 8), lpf=2400, drive=2/8, chop=var([0, 3, 1, 3], [4, 4]), room=3/5, mix=1/2, pan=(-2/3, 2/3), amplify=var([1, 0], [28, 4])*3/9, amp=var([1, 0, 1, 0, 1, 0], [64, 64, 96, 64, 32, 32]))
#SAMPLES
b1 >> play("V", dur=4, delay=[(0, 1, 3), 1, (0, 1+1/2, 3),  (1, 2+1/2)], sample=4, rate=3/4, shape=1/8, amplify=4/5, amp=var([0, 1, 0, 1, 0, 1, 0, 1, 0], [64, 96, 16, 1, 15, 1, 31, 96, 32]))
b2 >> play("X", dur=4, delay=[(0, 1, 3), 1, (0, 1+1/2, 3), (1, 2+1/2)], sample=4, rate=1, amplify=4/5, amp=var([0, 1, 0, 1, 0, 1, 0, 1, 0], [64, 96, 16, 1, 15, 1, 31, 96, 32]))
b3 >> play("V", dur=4, delay=[3, 1, (0, 3/2, 3), (0, 3/2, 3)], sample=8, pshift=var([12, 0, 7, 0], [5, 11]), amplify=var([0, 1], [3, 13])*3/4, amp=var([0, 1, 0, 1, 0], [64, 96, 64, 96, 32]))
b4 >> play("i", dur=4, delay=2, cut=1, sample=23, rate=7/5, pshift=-1, amplify=3/4, amp=var([0, 1, 0, 1, 0, 1, 0, 1, 0], [16, 32, 16, 96, 16, 32, 16, 96, 32]))
b5 >> play("i", dur=4, delay=2, cut=1/4, sample=14, amplify=1/3, amp=var([0, 1, 0, 1, 0], [64, 96, 64, 96, 32]))
b6 >> play("*", dur=1/4, sample=1, cut=0, amplify=var([0, 1, 0], [11+1/2, 1/2, 4])*1/3, amp=var([0, 1, 0, 1, 0], [64, 96, 64, 96, 32]))
b7 >> play("d", dur=8, delay=1+1/2, sample=12, rate=2, room=2/3, mix=1/3, amplify=4/5, amp=var([0, 1, 0, 1, 0], [64, 96, 80, 80, 32]))
b8 >> play("-", dur=1/2, sample=15, rate=6/5, pshift=1, amplify=var([1, 0, 1], [5, 1/2, 2+1/2])*4/5, amp=var([1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0], [48, 16, 16, 2, 30, 2, 30, 2, 62, 16, 16, 2, 30, 2, 30, 2, 14, 16, 16])).sometimes("stutter", 4).every(8, "stutter", delay=[1/12, 2/12, 2/6])
b9 >> play("#", dur=1/2, sample=27, rate=6/5, shape=1/3, amplify=var([0, 1], [3, 13])*4/8, amp=var([0, 1, 0, 1, 0], [64, 64, 96, 64, 64]))
b0 >> play("E", dur=32, sample=9, rate=1, hpf=2800, room=3/4, mix=1/2, pan=(-2/3, 2/3), amplify=6/5, amp=var([1, 0, 1, 0, 1, 0, 1], [32, 96, 64, 96, 32, 32]))
#ClicCloc
c1 >> play("k", dur=1, delay=[0, PRand([0, [1/2, 0], 1/2, [0, 1/2], 1/2, (0, 1/2)], seed=13)], rate=1, sample=16, pshift=[0, -6], verb=3/4, room=1/2, mix=1/3, amplify=var([0, 1, 0], [1+1/2, 6+1/2, 8]), amp=var([0, 1, 0, 1, 0], [64, 96, 64, 96, 32]))
c2 >> play("k", dur=1, rate=[1, 4/5], room=2/3, mix=1/3, amplify=1, amp=var([0, 1, 0, 1, 1], [64, 96, 64, 96, 32]))
#Transition
d1 >> play("*", dur=var([1, 1/2, 1/4], [16, 8, 8]), sample=1, amplify=var([1, 0], [30, 2])*4/5, amp=var([0, 1, 0, 1, 0, 0], [32, 32, 128, 32, 96, 32]))
d2 >> play("V", dur=var([2, 1], [24, 4, 4]), sample=4, rate=3/4, shape=1/8, amplify=var([0, 1, 0], [16, 14, 2])*4/5, amp=var([0, 1, 0, 1, 0, 0], [32, 32, 128, 32, 96, 32]))
d3 >> play("X", dur=var([2, 1], [24, 4, 4]), sample=4, rate=1, amplify=var([0, 1, 0], [16, 14, 2])*4/5, amp=var([0, 1, 0, 1, 0, 0], [32, 32, 128, 32, 96, 32]))
d4 >> play("E", dur=16, cut=0, sample=9, formant=5, rate=-3/5, shape=1/8, hpf=1900, room=3/4, mix=1/2, pan=(-2/3, 2/3), amplify=var([0, 1, 0], [16, 14, 2])*5/6, amp=var([0, 1, 0, 1, 0, 0], [32, 32, 128, 32, 96, 32]))
d5 >> play("V", dur=16, sample=4, rate=3/4, shape=1/8, amplify=4/5, amp=var([1, 0, 1, 0, 0], [64, 96, 64, 96, 32]))
d6 >> play("X", dur=16, sample=4, rate=1, amplify=4/5, amp=var([1, 0, 1, 0, 0], [64, 96, 64, 96, 32]))
#More SFX
f1 >> play("Z", dur=2, sample=6, rate=5/6, echo=13/12, echotime=8, pshift=12, room=5/6, mix=1/2, pan=(-2/3, 2/3), amplify=3/5, amp=var([0, 1, 0, 1, 0, 0], [62, 2, 190, 2, 96, 32]))
f2 >> play("F", dur=16, sample=8, cut=var([8, 6], 16), rate=1, pshift=var([3, 0], 16), room=5/6, mix=1/2, pan=(-2/3, 2/3), amplify=3/4, amp=var([0, 1, 0, 1, 0, 1, 0, 1], [32, 32, 64, 32, 32, 32, 64, 32]))
f3 >> play("!", dur=2, cut=2, sample=20, rate=6/5, pshift=-7, bits=8, room=2/3, mix=1/3, amplify=var([0, 1], [30, 2])*3, amp=var([0, 1, 0, 0], [32, 32, 256, 32]))
f4 >> play("!", dur=2, sample=10, drive=1/8, pshift=3, shape=1/7, amplify=var([0, 1], [30, 2])*7/5, amp=var([0, 1, 0, 0], [192, 32, 96, 32]))
f5 >> play("!", dur=1, sample=9, echo=1, echotime=8, rate=4/5, pshift=7, amplify=var([1, 0], [1, 15])*2, amp=var([0, 1, 0, 1, 0, 0], [96, 16, 144, 16, 48, 32]))
